#-------------------------------------------------
#
# Project created by QtCreator 2012-07-17T17:21:10
#
#-------------------------------------------------

CONFIG += qt debug qwt
QT += core gui network serialport widgets

TARGET = updateDVB
TEMPLATE = app

SOURCES += \
	main.cpp \
	views/main_window.cpp \
	views/demux_file_window.cpp \
	views/blindscan_save_dialog.cpp \
	views/settings_window.cpp \
	views/demux_dvr_dialog.cpp \
	threads/dvbtune_thread.cpp \
	threads/dvr_thread.cpp \
	threads/spectrumscan_thread.cpp \
	classes/dvbstream_class.cpp \
	classes/parsing_classes.cpp \
	classes/freq_list_class.cpp \
	classes/helper_classes.cpp \
	classes/dvb_settings.cpp \
	views/spectrumscan_tab.cpp \
	views_workers/spectrumscan_tab_worker.cpp \
	classes/master_class.cpp \
	views/adapter_tab.cpp \
	views_workers/tuning_tab_worker.cpp \
	views_workers/blindscan_tab_worker.cpp \
	views/tuning_tab.cpp \
	views/iqplot_tab.cpp \
	views/blindscan_tab.cpp \
	threads/tuning_tab_thread.cpp \
	threads/blindscan_tab_thread.cpp \
    classes/dvb_class.cpp


HEADERS += \
	classes/dvbstream_class.h \
	classes/parsing_classes.h \
	classes/freq_list_class.h \
	classes/helper_classes.h \
	classes/dvb_settings.h \
	classes/master_class.h \
	views/main_window.h \
	views/demux_file_window.h \
	views/blindscan_save_dialog.h \
	views/settings_window.h \
	views/demux_dvr_dialog.h \
	views/spectrumscan_tab.h \
	views/adapter_tab.h \
	views/blindscan_tab.h \
	views/iqplot_tab.h \
	views/tuning_tab.h \
	views_workers/spectrumscan_tab_worker.h \
	views_workers/blindscan_tab_worker.h \
	views_workers/tuning_tab_worker.h \
	threads/dvbtune_thread.h \
	threads/dvr_thread.h \
	threads/spectrumscan_thread.h \
	threads/blindscan_tab_thread.h \
	threads/tuning_tab_thread.h \
    classes/dvb_class.h

FORMS += \
	views/main_window.ui \
	views/demux_file_window.ui \
	views/blindscan_save_dialog.ui \
	views/settings_window.ui \
	views/demux_dvr_dialog.ui \
	views/spectrumscan_tab.ui \
	views/adapter_tab.ui \
	views/iqplot_tab.ui \
	views/tuning_tab.ui \
	views/blindscan_tab.ui

RESOURCES += darkorange/darkorange.qrc

INCLUDEPATH += /usr/include/qwt
#-------------------------------------------------
#
# attempt to make libqwt linking independent
# of the linux Qt env. Make sure
# 'CONFIG += qt debug qwt' is at the top of
# the .pro file to enable the QWT variableis.
#
#-------------------------------------------------
# debian
QT_INSTALL_ARCHDATA_T = $$[QT_INSTALL_ARCHDATA]
contains(QT_INSTALL_ARCHDATA_T, .*qt5){
linux: QWT_ROOT = /usr/lib/x86_64-linux-gnu/qt5
		include ( $${QWT_ROOT}/mkspecs/features/qwt.prf )
}
# unified
contains(QT_INSTALL_ARCHDATA_T, /opt/Qt/$${QT_VERSION}/gcc_64){
linux: QWT_ROOT = /usr/lib/x86_64-linux-gnu/qt5
		include ( $${QWT_ROOT}/mkspecs/features/qwt.prf )
}
# arch
contains(QT_INSTALL_ARCHDATA_T, /usr/lib/qt){
linux: QWT_ROOT = /usr/lib/qt
		include ( $${QWT_ROOT}/mkspecs/features/qwt.prf )
}
# rpi
contains(QMAKE_HOST.arch, armv.*){
linux: QWT_ROOT = /usr/lib/qt
		include ( $${QWT_ROOT}/mkspecs/features/qwt.prf )
}
message("QWT VERSION: 		"$${QWT_VERSION})
message("QT VERSION: 		"$${QT_VERSION})
message("QT INSTALL ARCHDATA: 	"$${QT_INSTALL_ARCHDATA_T})
message("Host Arch: 		"$${QMAKE_HOST.arch})
message("Host OS: 		"$${QMAKE_HOST.os})
message("HOST CPU CNT: 		"$${QMAKE_HOST.cpu_count})
message("HOST NAME: 		"$${QMAKE_HOST.name})
message("HOST VERSION: 		"$${QMAKE_HOST.version})
message("HOST VERSION STRING: 	"$${QMAKE_HOST.version_string})

QMAKE_LFLAGS += -no-pie
QMAKE_CXXFLAGS += -std=c++11

TARGET.path = /usr/local/bin
TARGET.files = updateDVB
TARGET.extra += cp updatedvb.desktop /usr/share/applications/ ;
TARGET.extra += cp updateDVB.png /usr/share/pixmaps/ ;
TARGET.extra += ln -sf /usr/local/bin/updateDVB ~/Desktop/updateDVB
INSTALLS += TARGET
