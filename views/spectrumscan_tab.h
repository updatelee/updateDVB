/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTRUMSCAN_TAB_H
#define SPECTRUMSCAN_TAB_H

#include <QWidget>
#include <QPen>
#include <qwt_plot.h>
#include <qwt_legend.h>
#include <qwt_plot_picker.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>
#include <qwt_picker_machine.h>
#include "classes/dvb_class.h"
#include "classes/master_class.h"
#include "classes/helper_classes.h"
#include "views_workers/spectrumscan_tab_worker.h"
#include "views/tuning_tab.h"
#include "views/blindscan_tab.h"

namespace Ui {
class spectrumscan_tab;
}

class PlotPicker : public QwtPlotPicker
{
public:
	PlotPicker(int xAxis, int yAxis, RubberBand rubberBand, DisplayMode trackerMode, QwtPlotCanvas* canvas) : QwtPlotPicker(xAxis, yAxis, rubberBand, trackerMode, canvas)
	{
	}

private:
	QwtText trackerText( const QPoint &pos ) const
	{
		return trackerTextF( invTransform( pos ) );
	}

	QwtText trackerTextF(const QPointF &pos) const
	{
		QwtText text(QString("%L1").arg(pos.x(), 0, 'f', 0) + " - " + QString("%L1").arg(pos.y(), 0, 'f', 0));
		return text;
	}
};

class spectrumscan_tab : public QWidget
{
	Q_OBJECT

public:
	explicit spectrumscan_tab(QWidget *parent, dvb_class *d);
	~spectrumscan_tab();

	void reload_lnb();
	void reload_settings();
	void setup_tuning_options();
	void adapter_status();
	void reload_qwtplot();
	QPixmap grab_qwt_plot();

	spectrumscan_tab_worker *worker;

private:
	Ui::spectrumscan_tab *ui;

	dvb_class *dvb;
	master_class *mc;

	dvb_settings dvbnames;
	bool noload;

	void add_comboBox_modulation(QString name);
	void set_colors(int lnb, int pol);
	void set_chart_details(int lnb, int pol);

signals:
	void new_tuning_tab();
	void new_blinds_tab(QString s);
	void ctrl_tab();

public slots:
	void qwt_draw(QVector<double> x, QVector<double> y, int min, int max, int lnb, int pol, unsigned int scale);
	void markers_draw(int lnb, int pol);
	void qwtPlot_selected(QPointF pos);
	void update_signal();
	void hide_signal();

private slots:
	void on_checkBox_loop_stateChanged();
	void on_pushButton_spectrumscan_clicked();
	void on_pushButton_blindscan_clicked();
	void on_pushButton_usals_go_clicked();
	void on_pushButton_gotox_go_clicked();
	void on_pushButton_gotox_save_clicked();
	void on_pushButton_drive_east_L_clicked();
	void on_pushButton_drive_east_S_clicked();
	void on_pushButton_drive_west_S_clicked();
	void on_pushButton_drive_west_L_clicked();
	void on_comboBox_frontend_currentIndexChanged(int index);
	void on_comboBox_lnb_currentIndexChanged(int index);
	void on_comboBox_polarity_currentIndexChanged(int index);
	void on_comboBox_modcod_currentIndexChanged(int index);
	void on_rb1_clicked();
	void on_rb2_clicked();
	void on_rb3_clicked();
	void on_rb4_clicked();
	void on_rb5_clicked();
	void on_pushButton_gotox_halt_clicked();
	void on_pb_gotox_limit_w_clicked();
	void on_pb_gotox_limits_off_clicked();
	void on_pb_gotox_limit_on_clicked();
	void on_pb_gotox_limit_e_clicked();

	void on_pb_gotox_sync_clicked();

protected:
	void keyPressEvent(QKeyEvent *event);
};

#endif // SPECTRUMSCAN_TAB_H
