/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "views/blindscan_tab.h"
#include "ui_blindscan_tab.h"

blindscan_tab::blindscan_tab(QWidget *parent, dvb_class *d) :
	QWidget(parent),
	ui(new Ui::blindscan_tab)
{
	dvb = d;
	worker = new blindscan_tab_worker(dvb);

	ui->setupUi(this);
	ui->pushButton_save->setEnabled(false);
	ui->pushButton_tune->setEnabled(false);

	pindex = -1;
	cindex = -1;

	mythread = new blindscan_tab_thread(dvb);
	connect(mythread, &blindscan_tab_thread::update_progress, this, &blindscan_tab::update_progress);
	connect(dvb->dvb_thread, &dvbtune_thread::update_signal, this, &blindscan_tab::update_signal, Qt::UniqueConnection);

	myprogress = new QProgressBar;
	myprogress->setMinimum(0);
	myprogress->setMaximum(100);
	myprogress->setVisible(true);
	ui->verticalLayout_progress->addWidget(myprogress);
}

blindscan_tab::~blindscan_tab()
{
	while (mythread->isRunning()) {
		mythread->loop = false;
		mythread->quit();
		mythread->wait(100);
	}
	mythread->deleteLater();

	worker->deleteLater();
	delete ui;
}

void blindscan_tab::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);

	// You have to give it some index
	emit tab_closed(-1);
}

void blindscan_tab::keyPressEvent(QKeyEvent *event)
{
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Tab) {
		emit ctrl_tab();
	}
	switch (event->key()) {
	case Qt::Key_Escape:
		emit tab_closed(-1);
		break;
	case Qt::Key_Enter:
	case Qt::Key_Return:
		on_pushButton_tune_clicked();
		break;
	default:
		break;
	}
}

void blindscan_tab::on_pushButton_tune_clicked()
{
	if (mythread->loop == true) {
		qDebug() << "Not done scanning yet";
		return;
	}
	int i = -1;
	for(int a = 0; a <= pindex; a++) {
		if (ptree[a]->isSelected()) {
			i = a;
		}
	}
	if (i == -1) {
		qDebug() << "No frequency selected";
		return;
	}

	dvb->dvb_thread->tp = mytp_info.at(i);
	emit new_tuning_tab();
}

void blindscan_tab::on_pushButton_save_clicked()
{
	blindscan_save_dialog mysave;
	mysave.mytp_info	= mytp_info;

	mysave.setModal(true);
	mysave.exec();
}

void blindscan_tab::on_pushButton_expand_clicked()
{
	for(int a = 0; a <= pindex; a++) {
		ptree[a]->setExpanded(true);
	}
}

void blindscan_tab::on_pushButton_unexpand_clicked()
{
	for(int a = 0; a <= pindex; a++) {
		ptree[a]->setExpanded(false);
	}
}

void blindscan_tab::start_scan()
{
	mythread->loop = false;
	mythread->thread_function.append(type);
	mythread->start();
}

void blindscan_tab::scan()
{
	type = "blindscan";
	start_scan();
}

void blindscan_tab::smartscan()
{
	type = "smartscan";
	start_scan();
}

void blindscan_tab::update_progress(int i)
{
	dvb->emit_update_status("", STATUS_CLEAR);
	if (isSatellite(dvb->dvb_thread->tp.system)) {
		dvb->emit_update_status("Tuning: " + QString::number(dvb->dvb_thread->tp.frequency) + " MHz", 0);
	} else {
		dvb->emit_update_status("Tuning: " + QString::number(dvb->dvb_thread->tp.frequency/1000) + " MHz", 0);
	}
	if (i > myprogress->maximum()) {
		i = myprogress->maximum();
	}
	myprogress->setValue(i);
	if (myprogress->value() >= myprogress->maximum()) {
		dvb->emit_update_status("", STATUS_CLEAR);
		myprogress->setVisible(false);
		ui->pushButton_save->setEnabled(true);
		ui->pushButton_tune->setEnabled(true);
		dvb->emit_update_status("Done...", 3);
	}
}

int blindscan_tab::tree_create_root(QString text)
{
	pindex++;
	ptree[pindex] = new QTreeWidgetItem(ui->treeWidget);
	ptree[pindex]->setText(0, text);

	return pindex;
}

int blindscan_tab::tree_create_child(int parent, QString text)
{
	cindex++;
	ctree[cindex] = new QTreeWidgetItem();
	ctree[cindex]->setText(0, text);
	ptree[parent]->addChild(ctree[cindex]);

	return cindex;
}

void blindscan_tab::update_signal()
{
	if (!(dvb->dvb_thread->tp.status & FE_HAS_LOCK)) {
		mythread->mutex.unlock();
		return;
	}
	if (!(dvb->dvb_thread->tp.status & (0xFF ^ FE_TIMEDOUT))) {
		mythread->mutex.unlock();
		return;
	}
	for (tp_info t : mytp_info) {
		if (t.frequency == dvb->dvb_thread->tp.frequency) {
			mythread->mutex.unlock();
			return;
		}
	}

	int parent_1;

	mytp_info.append(dvb->dvb_thread->tp);

	freq_list myfreq;
	QString text;
	if (isSatellite(dvb->dvb_thread->tp.system)) {
		text = QString::number(dvb->dvb_thread->tp.frequency) + dvbnames.voltage[dvb->dvb_thread->tp.polarity] + QString::number(dvb->dvb_thread->tp.symbolrate);
	} else if (isATSC(dvb->dvb_thread->tp.system)) {
		myfreq.atsc();
		text = QString::number(dvb->dvb_thread->tp.frequency/1000) + " MHz, Channel " + QString::number(myfreq.ch.at(myfreq.freq.indexOf(dvb->dvb_thread->tp.frequency)));
	} else if (isQAM(dvb->dvb_thread->tp.system)) {
		myfreq.qam();
		text = QString::number(dvb->dvb_thread->tp.frequency/1000) + " MHz, Channel " + QString::number(myfreq.ch.at(myfreq.freq.indexOf(dvb->dvb_thread->tp.frequency)));
	} else if (isDVBT(dvb->dvb_thread->tp.system)) {
		myfreq.dvbt();
		text = QString::number(dvb->dvb_thread->tp.frequency/1000) + " MHz, Channel " + QString::number(myfreq.ch.at(myfreq.freq.indexOf(dvb->dvb_thread->tp.frequency)));
	} else {
		text = QString::number(dvb->dvb_thread->tp.frequency/1000) + " MHz";
	}

	parent_1 = tree_create_root(text);
	if (dvb->dvb_thread->tp.status & FE_HAS_LOCK) {
		ptree[parent_1]->setForeground(0, QBrush(Qt::green));
	} else {
		ptree[parent_1]->setForeground(0, QBrush(Qt::red));
	}
	text = "System: " + dvbnames.system(dvb->dvb_thread->tp.system);
	tree_create_child(parent_1, text);
	text = "Modulation: " + dvbnames.modulation(dvb->dvb_thread->tp.modulation);
	tree_create_child(parent_1, text);

	if (isSatellite(dvb->dvb_thread->tp.system)) {
		text = "FEC: " + dvbnames.fec(dvb->dvb_thread->tp.fec);
		tree_create_child(parent_1, text);
		text = "Inversion: " + dvbnames.inversion(dvb->dvb_thread->tp.inversion);
		tree_create_child(parent_1, text);
		text = "Rolloff: " + dvbnames.rolloff(dvb->dvb_thread->tp.rolloff);
		tree_create_child(parent_1, text);
		text = "Pilot: " + dvbnames.pilot(dvb->dvb_thread->tp.pilot);
		tree_create_child(parent_1, text);
	}

	if (dvb->dvb_thread->tp.lvl_scale == FE_SCALE_DECIBEL) {
		text = "Signal S: " + QString::number(dvb->dvb_thread->tp.lvl, 'f', 1) + "dBm";
	} else {
		text = "Signal S: " + QString::number(dvb->dvb_thread->tp.lvl) + "%";
	}
	tree_create_child(parent_1, text);
	if (dvb->dvb_thread->tp.snr_scale == FE_SCALE_DECIBEL) {
		text = "Signal Q: " + QString::number(dvb->dvb_thread->tp.snr, 'f', 1) + "dB";
		tree_create_child(parent_1, text);
		ctree[cindex]->setToolTip(0, "min snr: " + QString::number(dvb->dvb_thread->min_snr(), 'f', 1) + "dB");
	} else {
		text = "Signal Q: " + QString::number(dvb->dvb_thread->tp.snr) + "%";
		tree_create_child(parent_1, text);
	}

	mythread->mutex.unlock();
}

void blindscan_tab::on_pushButton_stopstart_clicked()
{
	if (mythread->isRunning()) {
		while (mythread->isRunning()) {
			mythread->loop = false;
			mythread->quit();
			mythread->wait(100);
		}
		dvb->update_status("", STATUS_CLEAR);
		myprogress->setVisible(false);
		ui->pushButton_stopstart->setText("Start");
	} else {
		ui->treeWidget->clear();
		start_scan();
		ui->pushButton_stopstart->setText("Stop");
	}
}
