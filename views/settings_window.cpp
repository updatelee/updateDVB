/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings_window.h"
#include "ui_settings_window.h"

settings_window::settings_window(QWidget *parent, QVector<dvb_class *> d) :
	QDialog(parent),
	ui(new Ui::settings_window)
{
	nosave = true;
	ui->setupUi(this);

	dvb = d;
	mysettings = dvb.at(0)->mc->mysettings;

	if (!mysettings->value("adapter0_frontend0_name").isValid()) {
		QMessageBox welcome;
		welcome.setText("Welcome to updateDVB! \nPlease setup your LNB's then your devices before continuing");
		welcome.exec();
	}
}

settings_window::~settings_window()
{
	qDebug() << Q_FUNC_INFO;

	delete ui;
}

void settings_window::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);

	save_settings();
	this->deleteLater();
}

void settings_window::startup()
{
	noload = true;
	QVector<int> adaps;
	QDir dvb_dir("/dev/dvb");
	dvb_dir.setFilter(QDir::Dirs|QDir::NoDotAndDotDot);
	QStringList adapter_entries = dvb_dir.entryList();
	adapter_entries = adapter_entries.filter("adapter");
	for(int i = 0; i < adapter_entries.size(); i++) {
		QString adapter_name = adapter_entries.at(i);
		adapter_name.replace("adapter", "");
		adaps.append(adapter_name.toInt());
	}
	std::sort(adaps.begin(), adaps.end()) ;
	for (int i = 0; i < adaps.size(); i++) {
		if (!mysettings->value("adapter"+QString::number(adaps.at(i))+"_name").toString().isEmpty()) {
			ui->comboBox_adapter->insertItem(adaps.at(i), QString("%1 - %2").arg(adaps.at(i)).arg(mysettings->value("adapter"+QString::number(adaps.at(i))+"_name").toString()), adaps.at(i));
		} else {
			ui->comboBox_adapter->insertItem(adaps.at(i), QString("%1").arg(adaps.at(i)), adaps.at(i));
		}
	}
	for (int i = 0; i < MAX_LNBS; i++) {
		if (!mysettings->value("lnb"+QString::number(i)+"_name").toString().isEmpty()) {
			ui->comboBox_lnb->insertItem(i, QString("%1 - %2").arg(i).arg(mysettings->value("lnb"+QString::number(i)+"_name").toString()) , i);
			ui->comboBox_default_lnb->insertItem(i, QString("%1 - %2").arg(i).arg(mysettings->value("lnb"+QString::number(i)+"_name").toString()) , i);
		} else {
			ui->comboBox_lnb->insertItem(i, QString("%1").arg(i) , i);
			ui->comboBox_default_lnb->insertItem(i, QString("%1").arg(i) , i);
		}
	}

	noload = false;

	ui->comboBox_lnb->setCurrentIndex(0);
	ui->progressBar->hide();
	load_settings();
	nosave = false;
}

void settings_window::set_player()
{
	QFile tmp;
	if (tmp.exists("/usr/bin/omxplayer.bin")) {
		ui->lineEdit_play->setText("lxterminal -e omxplayer.bin --win 0,28,1000,565 /dev/dvb/adapter{}/dvr0");
		return;
	}
	if (tmp.exists("/usr/bin/vlc")) {
		ui->lineEdit_play->setText("/usr/bin/vlc dvb://dev/dvb/adapter{}/dvr0");
		return;
	}
	if (tmp.exists("/usr/bin/mpv")) {
		ui->lineEdit_play->setText("/usr/bin/mpv /dev/dvb/adapter{}/dvr0");
		return;
	}
}

void settings_window::load_settings()
{
	if (noload) {
		return;
	}

	save_settings();

	lnb = ui->comboBox_lnb->currentIndex();
	adp = ui->comboBox_adapter->currentData().toInt();
	fnd = ui->comboBox_frontend->currentData().toInt();

	ui->lineEdit_lat->setText(mysettings->value("site_lat").toString());
	ui->lineEdit_long->setText(mysettings->value("site_long").toString());
	ui->lineEdit_asc1_serialport->setText(mysettings->value("asc1_serialport").toString());

	ui->lineEdit_loop_delay->setText(QString::number(mysettings->value("adapter"+QString::number(adp)+"_loop_delay").toInt()));

	ui->checkBox_diseqc_v13->setChecked(mysettings->value("adapter"+QString::number(adp)+"_diseqc_v13").toBool());
	on_checkBox_diseqc_v13_clicked();
	ui->checkBox_diseqc_v12->setChecked(mysettings->value("adapter"+QString::number(adp)+"_diseqc_v12").toBool());
	on_checkBox_diseqc_v12_clicked();

	ui->checkBox_servo->setChecked(mysettings->value("adapter"+QString::number(adp)+"_servo").toBool());
	ui->checkBox_asc1->setChecked(mysettings->value("adapter"+QString::number(adp)+"_asc1").toBool());

	if (mysettings->value("adapter"+QString::number(adp)+"_name").toString() != "") {
		ui->lineEdit_adapter_name->setText(mysettings->value("adapter"+QString::number(adp)+"_name").toString());
	} else {
		ui->lineEdit_adapter_name->setText(dvb.at(ui->comboBox_adapter->currentIndex())->dvb_thread->name);
	}

	on_checkBox_asc1_clicked();
	for (int i = 1; i < 256; i++) {
		ui->tableWidget_diseqc_v12->setItem(i-1, 0, new QTableWidgetItem(mysettings->value("adapter"+QString::number(adp)+"_diseqc_v12_name_"+QString::number(i)).toString()));
		ui->tableWidget_diseqc_v12->setItem(i-1, 1, new QTableWidgetItem(mysettings->value("adapter"+QString::number(adp)+"_diseqc_v12_pos_"+QString::number(i)).toString()));
		ui->tableWidget_diseqc_v12->setItem(i-1, 2, new QTableWidgetItem(""));
		ui->tableWidget_diseqc_v12->setItem(i-1, 3, new QTableWidgetItem(""));
		ui->tableWidget_diseqc_v12->setItem(i-1, 4, new QTableWidgetItem(""));
	}

	if (mysettings->value("lnb"+QString::number(lnb)+"_enabled").isValid()) {
		ui->checkBox_enabled->setChecked(mysettings->value("lnb"+QString::number(lnb)+"_enabled").toBool());
	}
	ui->lineEdit_lnb_name->setText(mysettings->value("lnb"+QString::number(lnb)+"_name").toString());
	ui->checkBox_tone->setChecked(mysettings->value("lnb"+QString::number(lnb)+"_tone").toBool());

	ui->comboBox_committed->setCurrentIndex(mysettings->value("lnb"+QString::number(lnb)+"_committed").toInt());
	ui->comboBox_uncommitted->setCurrentIndex(mysettings->value("lnb"+QString::number(lnb)+"_uncommitted").toInt());

	int t_index = ui->comboBox_type->findText(mysettings->value("lnb"+QString::number(lnb)+"_type").toString());
	if (t_index >= 0) {
		ui->comboBox_type->setCurrentIndex(t_index);
	}

	if (mysettings->value("lnb"+QString::number(lnb)+"_freqstop").toString().isEmpty()) {
		on_comboBox_type_currentIndexChanged(ui->comboBox_type->currentIndex());
	} else {
		ui->lineEdit_f_lof->setText(mysettings->value("lnb"+QString::number(lnb)+"_freqlof").toString());
		ui->lineEdit_f_start->setText(mysettings->value("lnb"+QString::number(lnb)+"_freqstart").toString());
		ui->lineEdit_f_stop->setText(mysettings->value("lnb"+QString::number(lnb)+"_freqstop").toString());
		ui->comboBox_voltage->setCurrentIndex(mysettings->value("lnb"+QString::number(lnb)+"_voltage").toInt());
	}

	if (mysettings->value("cmd_play").toString() != "") {
		ui->lineEdit_play->setText(mysettings->value("cmd_play").toString());
	} else {
		set_player();
	}

	if (mysettings->value("savelocation").toString() != "") {
		ui->lineEdit_savelocation->setText(mysettings->value("savelocation").toString());
	} else {
		ui->lineEdit_savelocation->setText(QDir::homePath() + "/Desktop");
	}

	ui->comboBox_default_lnb->setCurrentIndex(mysettings->value("adapter"+QString::number(adp)+"_default_lnb").toInt());

	if (mysettings->value("motor_delay").toUInt() != 0) {
		ui->lineEdit_motor_delay->setText(mysettings->value("motor_delay").toString());
	} else {
		ui->lineEdit_motor_delay->setText("500");
	}

}

void settings_window::save_settings()
{
	if (nosave) {
		return;
	}

	mysettings->setValue("adapter"+QString::number(adp)+"_diseqc_v12", ui->checkBox_diseqc_v12->isChecked());
	mysettings->setValue("adapter"+QString::number(adp)+"_diseqc_v13", ui->checkBox_diseqc_v13->isChecked());
	mysettings->setValue("adapter"+QString::number(adp)+"_name", ui->lineEdit_adapter_name->text());
	mysettings->setValue("adapter"+QString::number(adp)+"_frontend"+QString::number(fnd)+"_name", ui->lineEdit_frontend_name->text());
	mysettings->setValue("adapter"+QString::number(adp)+"_default_lnb", ui->comboBox_default_lnb->currentIndex());
	mysettings->setValue("adapter"+QString::number(adp)+"_loop_delay", ui->lineEdit_loop_delay->text());

	for (int i = 1; i < 100; i++) {
		mysettings->setValue("adapter"+QString::number(adp)+"_diseqc_v12_name_"+QString::number(i), ui->tableWidget_diseqc_v12->item(i-1, 0)->text());
		mysettings->setValue("adapter"+QString::number(adp)+"_diseqc_v12_pos_"+QString::number(i), ui->tableWidget_diseqc_v12->item(i-1, 1)->text());
	}

	mysettings->setValue("lnb"+QString::number(lnb)+"_name", ui->lineEdit_lnb_name->text());
	mysettings->setValue("lnb"+QString::number(lnb)+"_enabled", ui->checkBox_enabled->isChecked());
	mysettings->setValue("lnb"+QString::number(lnb)+"_tone", ui->checkBox_tone->isChecked());
	mysettings->setValue("lnb"+QString::number(lnb)+"_voltage", ui->comboBox_voltage->currentIndex());

	mysettings->setValue("lnb"+QString::number(lnb)+"_committed", ui->comboBox_committed->currentIndex());
	mysettings->setValue("lnb"+QString::number(lnb)+"_uncommitted", ui->comboBox_uncommitted->currentIndex());

	mysettings->setValue("lnb"+QString::number(lnb)+"_type", ui->comboBox_type->currentText());

	mysettings->setValue("lnb"+QString::number(lnb)+"_freqlof", ui->lineEdit_f_lof->text().toInt());
	mysettings->setValue("lnb"+QString::number(lnb)+"_freqstart", ui->lineEdit_f_start->text().toInt());
	mysettings->setValue("lnb"+QString::number(lnb)+"_freqstop", ui->lineEdit_f_stop->text().toInt());

	mysettings->setValue("cmd_play", ui->lineEdit_play->text());
	mysettings->setValue("savelocation", ui->lineEdit_savelocation->text());

	mysettings->setValue("site_lat", ui->lineEdit_lat->text());
	mysettings->setValue("site_long", ui->lineEdit_long->text());
	mysettings->setValue("asc1_serialport", ui->lineEdit_asc1_serialport->text());

	mysettings->setValue("adapter"+QString::number(adp)+"_asc1", ui->checkBox_asc1->isChecked());
	mysettings->setValue("adapter"+QString::number(adp)+"_servo", ui->checkBox_servo->isChecked());

	mysettings->sync();

	lnb = ui->comboBox_lnb->currentIndex();
	adp = ui->comboBox_adapter->currentData().toInt();

	for (int i = 0; i < MAX_LNBS; i++) {
		if (!mysettings->value("lnb"+QString::number(i)+"_name").toString().isEmpty()) {
			ui->comboBox_lnb->setItemText(i, QString("%1 - %2").arg(i).arg(mysettings->value("lnb"+QString::number(i)+"_name").toString()));
			ui->comboBox_default_lnb->setItemText(i, QString("%1 - %2").arg(i).arg(mysettings->value("lnb"+QString::number(i)+"_name").toString()));
		} else {
			ui->comboBox_lnb->setItemText(i, QString("%1").arg(i));
			ui->comboBox_default_lnb->setItemText(i, QString("%1").arg(i));
		}
	}
	ui->comboBox_default_lnb->setCurrentIndex(mysettings->value("adapter"+QString::number(adp)+"_default_lnb").toInt());

	if (!mysettings->value("adapter"+QString::number(adp)+"_name").toString().isEmpty()) {
		ui->comboBox_adapter->setItemText(ui->comboBox_adapter->currentIndex(), QString("%1 - %2").arg(adp).arg(mysettings->value("adapter"+QString::number(adp)+"_name").toString()));
	} else {
		ui->comboBox_adapter->setItemText(ui->comboBox_adapter->currentIndex(), QString("%1").arg(adp));
	}

	mysettings->setValue("motor_delay", ui->lineEdit_motor_delay->text());
}

void settings_window::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape) {
		this->close();
	}
}


void settings_window::on_comboBox_lnb_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	load_settings();
}

void settings_window::on_comboBox_adapter_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	load_settings();

	ui->comboBox_frontend->clear();
	QDir adapter_dir("/dev/dvb/adapter" + ui->comboBox_adapter->currentData().toString());
	adapter_dir.setFilter(QDir::System|QDir::NoDotAndDotDot);
	QStringList frontend_entries = adapter_dir.entryList();
	frontend_entries = frontend_entries.filter("frontend");
	for(int i = 0; i < frontend_entries.size(); i++) {
		QString frontend_name = frontend_entries.at(i);
		frontend_name.replace("frontend", "");
		ui->comboBox_frontend->insertItem(frontend_name.toInt(), frontend_name, frontend_name.toInt());
	}
	dvb.at(0)->emit_update_status(dvb.at(ui->comboBox_adapter->currentIndex())->dvb_thread->name, 5);
}

void settings_window::on_comboBox_type_currentIndexChanged(int index)
{
	Q_UNUSED(index);

	if (!mysettings->value("lnb" + QString::number(ui->comboBox_lnb->currentIndex()) + "_name").isValid()) {
		ui->lineEdit_lnb_name->setText(ui->comboBox_type->currentText());
	}

	if (ui->comboBox_type->currentText() == "C-Band") {
		ui->lineEdit_f_lof->setText("-5150");
		ui->lineEdit_f_start->setText("3660");
		ui->lineEdit_f_stop->setText("4240");
	}
	if (ui->comboBox_type->currentText() == "C-Band: Extended") {
		ui->lineEdit_f_lof->setText("-5150");
		ui->lineEdit_f_start->setText("3160");
		ui->lineEdit_f_stop->setText("4240");
	}
	if (ui->comboBox_type->currentText() == "Ku Linear") {
		ui->lineEdit_f_lof->setText("10750");
		ui->lineEdit_f_start->setText("11660");
		ui->lineEdit_f_stop->setText("12240");
	}
	if (ui->comboBox_type->currentText() == "Ku Circular") {
		ui->lineEdit_f_lof->setText("11250");
		ui->lineEdit_f_start->setText("11160");
		ui->lineEdit_f_stop->setText("12740	");
	}
	if (ui->comboBox_type->currentText() == "Universal Low") {
		ui->lineEdit_f_lof->setText("9750");
		ui->lineEdit_f_start->setText("10660");
		ui->lineEdit_f_stop->setText("11240");
	}
	if (ui->comboBox_type->currentText() == "Universal High") {
		ui->lineEdit_f_lof->setText("10600");
		ui->lineEdit_f_start->setText("11510");
		ui->lineEdit_f_stop->setText("12090");
	}
	if (ui->comboBox_type->currentText() == "Universal Low: Extended") {
		ui->lineEdit_f_lof->setText("9750");
		ui->lineEdit_f_start->setText("10680");
		ui->lineEdit_f_stop->setText("11740");
	}
	if (ui->comboBox_type->currentText() == "Universal High: Extended") {
		ui->lineEdit_f_lof->setText("10600");
		ui->lineEdit_f_start->setText("11660");
		ui->lineEdit_f_stop->setText("12790");
	}
	if (ui->comboBox_type->currentText() == "ATSC") {
		ui->lineEdit_f_lof->setText("0");
		ui->lineEdit_f_start->setText("52");
		ui->lineEdit_f_stop->setText("610");
		ui->comboBox_voltage->setCurrentIndex(2);
	}
	if (ui->comboBox_type->currentText() == "QAM") {
		ui->lineEdit_f_lof->setText("0");
		ui->lineEdit_f_start->setText("52");
		ui->lineEdit_f_stop->setText("900");
		ui->comboBox_voltage->setCurrentIndex(2);
	}
	if (ui->comboBox_type->currentText() == "DVBT") {
		ui->lineEdit_f_lof->setText("0");
		ui->lineEdit_f_start->setText("174");
		ui->lineEdit_f_stop->setText("790");
		ui->comboBox_voltage->setCurrentIndex(2);
	}
	if (ui->comboBox_type->currentText() == "Custom") {
		ui->lineEdit_f_lof->setText("0");
		ui->lineEdit_f_start->setText("950");
		ui->lineEdit_f_stop->setText("1450");
	}
}

void settings_window::on_pushButton_save_clicked()
{
	save_settings();
}

void settings_window::on_pushButton_close_clicked()
{
	this->close();
}

void settings_window::on_checkBox_diseqc_v13_clicked()
{
	if (ui->checkBox_diseqc_v13->isChecked()) {
		ui->label_lat->show();
		ui->lineEdit_lat->show();
		ui->label_long->show();
		ui->lineEdit_long->show();
	} else {
		ui->label_lat->hide();
		ui->lineEdit_lat->hide();
		ui->label_long->hide();
		ui->lineEdit_long->hide();
	}
	if (ui->checkBox_diseqc_v12->isChecked() || ui->checkBox_diseqc_v13->isChecked()) {
		ui->label_motor_delay->show();
		ui->lineEdit_motor_delay->show();
	} else {
		ui->label_motor_delay->hide();
		ui->lineEdit_motor_delay->hide();
	}
}

void settings_window::on_checkBox_diseqc_v12_clicked()
{
	if (ui->checkBox_diseqc_v12->isChecked()) {
		ui->tableWidget_diseqc_v12->show();
		ui->tableWidget_diseqc_v12->setColumnCount(2);
		ui->tableWidget_diseqc_v12->setColumnWidth(0, 175);
		ui->tableWidget_diseqc_v12->setColumnWidth(1, 115);
		ui->checkBox_asc1->show();
		on_checkBox_asc1_clicked();
	} else {
		ui->tableWidget_diseqc_v12->hide();
		ui->checkBox_asc1->hide();
		on_checkBox_asc1_clicked();
	}
	if (ui->checkBox_diseqc_v12->isChecked() || ui->checkBox_diseqc_v13->isChecked()) {
		ui->label_motor_delay->show();
		ui->lineEdit_motor_delay->show();
	} else {
		ui->label_motor_delay->hide();
		ui->lineEdit_motor_delay->hide();
	}
}

void settings_window::on_checkBox_asc1_clicked()
{
	if (ui->checkBox_asc1->isChecked() && ui->checkBox_diseqc_v12->isChecked()) {
		ui->gridWidget_asc1->show();
		ui->tableWidget_diseqc_v12->setColumnCount(5);
		ui->tableWidget_diseqc_v12->setHorizontalHeaderLabels(QStringList() << "Name" << "Orbital Pos" << "Counter" << "V Deg" << "H Deg");
		ui->tableWidget_diseqc_v12->setColumnWidth(0, 175);
		ui->tableWidget_diseqc_v12->setColumnWidth(1, 115);
		ui->tableWidget_diseqc_v12->setColumnWidth(2, 75);
		ui->tableWidget_diseqc_v12->setColumnWidth(3, 55);
		ui->tableWidget_diseqc_v12->setColumnWidth(4, 55);
	} else {
		ui->gridWidget_asc1->hide();
		ui->tableWidget_diseqc_v12->setColumnCount(2);
		ui->tableWidget_diseqc_v12->setRowCount(256);
	}
}

void settings_window::on_pushButton_asc1_upload_clicked()
{
	QSerialPort myserial;
	QByteArray cmd;
	QByteArray data;
    QElapsedTimer t;
	t.start();

	myserial.setPortName(ui->lineEdit_asc1_serialport->text());
	myserial.open(QIODevice::ReadWrite);
	myserial.setBaudRate(QSerialPort::Baud57600);
	myserial.setDataBits(QSerialPort::Data8);
	myserial.setParity(QSerialPort::NoParity);
	myserial.setStopBits(QSerialPort::OneStop);
	myserial.setFlowControl(QSerialPort::NoFlowControl);

	if (!myserial.isOpen()) {
		qDebug() << "unable to open";
		return;
	}

	cmd.clear();
	cmd.append(0x4a);
	myserial.write(cmd);
	myserial.waitForBytesWritten(1000);

	t.restart();
	data.clear();
	myserial.waitForReadyRead(1000);
	data += myserial.readAll();
	while (data.isEmpty() || (data.at(0) != 0x4b && t.elapsed() < 1000)) {
		QThread::msleep(10);
		myserial.waitForReadyRead(1000);
		data += myserial.readAll();
	}
	if (data.at(0) != 0x4b) {
		qDebug() << "timeout...";
		return;
	}

	ui->progressBar->show();
	for (int i = 1; i < 100; i++) {
		cmd.clear();
		cmd.append((char)0x4c);
		cmd.append((char)i);
		QString tmp = ui->tableWidget_diseqc_v12->item(i-1, 0)->text();
		tmp += QString(16, 0x00);
		tmp.resize(16);
		cmd.append(tmp.toUtf8());
		cmd.append((char)0x00);
		cmd.append((char)(ui->tableWidget_diseqc_v12->item(i-1, 1)->text().toUInt() & 0xff));
		cmd.append((char)((ui->tableWidget_diseqc_v12->item(i-1, 1)->text().toUInt() >> 8) & 0xff));
		cmd.append((char)ui->tableWidget_diseqc_v12->item(i-1, 2)->text().toShort());
		cmd.append((char)ui->tableWidget_diseqc_v12->item(i-1, 3)->text().toShort());

		myserial.write(cmd);
		myserial.flush();
		myserial.waitForBytesWritten(1000);

		t.restart();
		data.clear();
		myserial.waitForReadyRead(1000);
		data += myserial.readAll();
		while (data.isEmpty() || (data.at(0) != 0x4b && t.elapsed() < 1000)) {
			QThread::msleep(10);
			myserial.waitForReadyRead(1000);
			data += myserial.readAll();
		}
		if (data.at(0) != 0x4b) {
			qDebug() << "timeout...";
			continue;
		}

		ui->progressBar->setValue(i);
	}
	ui->progressBar->hide();

	myserial.close();
}

void settings_window::on_pushButton_asc1_download_clicked()
{
	asc1_data mydata;
	QSerialPort myserial;
	QByteArray cmd;
	QByteArray data;
    QElapsedTimer t;
	t.start();

	myserial.setPortName(ui->lineEdit_asc1_serialport->text());
	myserial.open(QIODevice::ReadWrite);
	myserial.setBaudRate(QSerialPort::Baud57600);
	myserial.setDataBits(QSerialPort::Data8);
	myserial.setParity(QSerialPort::NoParity);
	myserial.setStopBits(QSerialPort::OneStop);
	myserial.setFlowControl(QSerialPort::NoFlowControl);

	if (!myserial.isOpen()) {
		qDebug() << "unable to open";
		return;
	}

	cmd.clear();
	cmd.append(0x48);
	myserial.write(cmd);
	myserial.flush();
	myserial.waitForBytesWritten(1000);

	data.clear();
	myserial.waitForReadyRead(1000);
	data += myserial.readAll();

	ui->progressBar->show();
	for (int i = 1; i < 100; i++) {
		QThread::msleep(5);

		cmd.clear();
		cmd.append(0x49);
		myserial.write(cmd);
		myserial.flush();
		myserial.waitForBytesWritten(1000);

		t.restart();

		data.clear();
		myserial.waitForReadyRead(1000);
		data += myserial.readAll();
		while (data.size() < 23 && t.elapsed() < 1000) {
			myserial.waitForReadyRead(1000);
			data += myserial.readAll();
		}

		if (data.size() < 23) {
			qDebug() << "timeout..." << t.elapsed();
			continue;
		}

		mydata.name		= data.mid(2, 16).data();
		mydata.counter	= (u_int8_t)data.mid(19, 1).at(0) << 8 | (u_int8_t)data.mid(20, 1).at(0);
		mydata.Hdeg		= (int8_t)data.mid(21, 1).at(0);
		mydata.Vdeg		= (int8_t)data.mid(22, 1).at(0);

		ui->tableWidget_diseqc_v12->setItem(i-1, 0, new QTableWidgetItem(mydata.name));
		ui->tableWidget_diseqc_v12->setItem(i-1, 1, new QTableWidgetItem(QString::number(mydata.counter)));
		ui->tableWidget_diseqc_v12->setItem(i-1, 2, new QTableWidgetItem(QString::number(mydata.Hdeg)));
		ui->tableWidget_diseqc_v12->setItem(i-1, 3, new QTableWidgetItem(QString::number(mydata.Vdeg)));

		ui->progressBar->setValue(i);
	}
	ui->progressBar->hide();

	myserial.close();
}

void settings_window::on_comboBox_frontend_currentIndexChanged(int index)
{
	load_settings();

	if (adp < 0) {
		adp = ui->comboBox_adapter->currentData().toInt();
	}

	ui->lineEdit_frontend_name->setText(mysettings->value("adapter"+QString::number(adp)+"_frontend"+QString::number(index)+"_name").toString());
}

void settings_window::on_pushButton_play_browse_clicked()
{
	QString ppath = ui->lineEdit_play->text().section(' ', 0, 0);
	if (ppath.isEmpty()) {
		ppath = QFileDialog::getOpenFileName(this, "Select Media Player", "/usr/bin");
	} else {
		ppath = QFileDialog::getOpenFileName(this, "Select Media Player", ppath);
	}
	if (ppath.isEmpty()) {
		return;
	}
	ui->lineEdit_play->setText(ppath + " /dev/dvb/adapter{}/dvr0");
}

void settings_window::on_pushButton_savelocation_clicked()
{
	QString ppath = ui->lineEdit_savelocation->text().section(' ', 0, 0);
	if (ppath.isEmpty()) {
		ppath = QFileDialog::getOpenFileName(this, "Select Default Save Location", QDir::homePath());
	} else {
		ppath = QFileDialog::getOpenFileName(this, "Select Default Save Location", ppath);
	}
	if (ppath.isEmpty()) {
		return;
	}
	ui->lineEdit_savelocation->setText(ppath);
}

void settings_window::on_tabWidget_currentChanged(int index)
{
	Q_UNUSED(index);
	save_settings();
}
