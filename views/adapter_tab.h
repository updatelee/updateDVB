/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADAPTER_TAB_H
#define ADAPTER_TAB_H

#include <QtCore>
#include <QWidget>
#include "views/spectrumscan_tab.h"
#include "views/blindscan_tab.h"
#include "classes/dvb_class.h"

namespace Ui {
class adapter_tab;
}

class adapter_tab : public QWidget
{
	Q_OBJECT
public:
	explicit adapter_tab(QWidget *parent, dvb_class *d);
	~adapter_tab();

	void reload_lnb();
	QPixmap grab_qwt_plot();

	dvb_class *dvb;

	spectrumscan_tab *myspectrumscan_tab;

private:
	Ui::adapter_tab *ui;

	QPointer<iqplot_tab> myiqplot_tab;
	QPointer<tuning_tab> mytuning_tab;
	QPointer<blindscan_tab> myblindscan_tab;

public slots:
	void tab_closed(int index);
	void ctrl_tab();
	void new_tuning_tab();
	void new_iqplot_tab();
	void new_blinds_tab(QString s);
	void update_signal();
};

#endif // ADAPTER_TAB_H
