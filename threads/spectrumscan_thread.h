/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTRUMSCAN_THREAD_H
#define SPECTRUMSCAN_THREAD_H

#include <QDebug>
#include <QThread>
#include <QVector>
#include <algorithm>    // std::sort
#include "classes/dvb_class.h"

class spectrumscan_thread : public QThread
{
	Q_OBJECT
signals:
	void signaldraw(QVector<double> x, QVector<double> y, int min, int max, int lnb, int pol, unsigned int scale);
	void update_status(QString text, int time);
	void markers_draw(int lnb, int pol);

public:
	spectrumscan_thread(dvb_class *p);
	~spectrumscan_thread();

	void run();
	bool loop;
	int lnb;

	waitout mutex;
	dvb_class *dvb;
	unsigned int points;
	unsigned int step;
	int min, max, min_old, max_old;
	QVector<double> x;
	QVector<double> y;
	int loop_delay;

	void sweep();
	void rescale();
private:
	struct dvb_fe_spectrum_scan fe_scan;
	dvb_settings dvbnames;
	int64_t f_start, f_stop;
};

#endif // SPECTRUMSCAN_THREAD_H
