/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dvr_thread.h"

dvr_thread::dvr_thread()
{
	loop	= false;
	file_fd	= -1;
}

dvr_thread::~dvr_thread()
{
	while (mydvbtune_thread->isRunning()) {
		mydvbtune_thread->loop = false;
		mydvbtune_thread->quit();
		mydvbtune_thread->wait(100);
	}
}

void dvr_thread::run()
{
	loop = true;
	while (loop) {
		if (thread_function.contains("demux_file")) {
			demux_file();
		}
		if (thread_function.contains("demux_stream")) {
			demux_stream();
		}
		if (thread_function.isEmpty() && loop) {
			msleep(100);
		}
	}
	thread_function.clear();
	close_file();
}

void dvr_thread::close_file()
{
	if (file_fd > 0) {
		close(file_fd);
		file_fd = -1;
		file_name.clear();
	}
}

void dvr_thread::demux_file()
{
	if (!mydvbtune_thread->open_dvr()) {
		return;
	}
	if (file_fd < 0) {
		file_fd = open(file_name.toStdString().c_str(), O_CREAT|O_TRUNC|O_RDWR|O_NONBLOCK, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
		if (file_fd < 0) {
			qDebug() << Q_FUNC_INFO << "Failed to open" << file_name;
			return;
		}
	}

	fd_set set;
	FD_ZERO(&set);
	FD_SET(mydvbtune_thread->dvr_fd, &set);

	int len = -1;
	char buf[LIL_BUFSIZE];
	memset(buf, 0, LIL_BUFSIZE);

	mydvbtune_thread->setbit(TUNER_RDING);
	if (select(mydvbtune_thread->dvr_fd + 1, &set, NULL, NULL, &mydvbtune_thread->fd_timeout) > 0) {
		len = read(mydvbtune_thread->dvr_fd, buf, LIL_BUFSIZE);
	}
	mydvbtune_thread->unsetbit(TUNER_RDING);
	if (len > 0) {
		ssize_t wlen = write(file_fd, buf, len);
		Q_UNUSED(wlen);
	}

	emit data_size(len);
}

void dvr_thread::demux_stream()
{
	if (!mydvbtune_thread->open_dvr()) {
		return;
	}

	fd_set set;
	FD_ZERO(&set);
	FD_SET(mydvbtune_thread->dvr_fd, &set);

	int len = -1;
	char buf[LIL_BUFSIZE];
	memset(buf, 0, LIL_BUFSIZE);

	mydvbtune_thread->setbit(TUNER_RDING);
	if (select(mydvbtune_thread->dvr_fd + 1, &set, NULL, NULL, &mydvbtune_thread->fd_timeout) > 0) {
		len = read(mydvbtune_thread->dvr_fd, buf, LIL_BUFSIZE);
	}
	mydvbtune_thread->unsetbit(TUNER_RDING);
	if (len > 0) {
		emit data(QByteArray(buf, len));
	}

	if (len != LIL_BUFSIZE) {
		qDebug() << Q_FUNC_INFO << "read issue:" << len << "of" << LIL_BUFSIZE;
		if (len == 0) {
			qDebug() << Q_FUNC_INFO << "reached end of file";
		} else if (len < 0) {
			qDebug() << Q_FUNC_INFO << "read error:" << strerror(errno);
		}
	}
}
