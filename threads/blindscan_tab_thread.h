/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLINDSCAN_TAB_THREAD_H
#define BLINDSCAN_TAB_THREAD_H

#include <QThread>
#include <QElapsedTimer>
#include <QDebug>
#include "classes/dvb_class.h"
#include "classes/helper_classes.h"
#include "views/settings_window.h"
#include "views/blindscan_save_dialog.h"

class blindscan_tab_thread : public QThread
{
	Q_OBJECT
public:
	blindscan_tab_thread(dvb_class *d);
	~blindscan_tab_thread();

	void run();
	bool loop;
	QVector<QString> thread_function;

	dvb_class *dvb;
	waitout mutex;
signals:
	void update_progress(int i);
private:
	void blindscan();
	void smartscan();
};

#endif // BLINDSCAN_TAB_THREAD_H
