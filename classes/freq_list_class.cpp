/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "freq_list_class.h"

freq_list::freq_list() {
	ch.clear();
	freq.clear();
}

void freq_list::atsc() {
	ch.clear();		freq.clear();
	ch.append(2);	freq.append(57028);
	ch.append(3);	freq.append(63028);
	ch.append(4);	freq.append(69028);
	ch.append(5);	freq.append(79028);
	ch.append(6);	freq.append(85028);
	ch.append(7);	freq.append(177028);
	ch.append(8);	freq.append(183028);
	ch.append(9);	freq.append(189028);
	ch.append(10);	freq.append(195028);
	ch.append(11);	freq.append(201028);
	ch.append(12);	freq.append(207028);
	ch.append(13);	freq.append(213028);
	ch.append(14);	freq.append(473028);
	ch.append(15);	freq.append(479028);
	ch.append(16);	freq.append(485028);
	ch.append(17);	freq.append(491028);
	ch.append(18);	freq.append(497028);
	ch.append(19);	freq.append(503028);
	ch.append(20);	freq.append(509028);
	ch.append(21);	freq.append(515028);
	ch.append(22);	freq.append(521028);
	ch.append(23);	freq.append(527028);
	ch.append(24);	freq.append(533028);
	ch.append(25);	freq.append(539028);
	ch.append(26);	freq.append(545028);
	ch.append(27);	freq.append(551028);
	ch.append(28);	freq.append(557028);
	ch.append(29);	freq.append(563028);
	ch.append(30);	freq.append(569028);
	ch.append(31);	freq.append(575028);
	ch.append(32);	freq.append(581028);
	ch.append(33);	freq.append(587028);
	ch.append(34);	freq.append(593028);
	ch.append(35);	freq.append(599028);
	ch.append(36);	freq.append(605028);
}

void freq_list::qam() {
	ch.clear();		freq.clear();
	ch.append(2);	freq.append(57000);
	ch.append(3);	freq.append(63000);
	ch.append(4);	freq.append(69000);
	ch.append(1);	freq.append(75000);
	ch.append(5);	freq.append(79000);
	ch.append(6);	freq.append(85000);
	ch.append(95);	freq.append(93000);
	ch.append(96);	freq.append(99000);
	ch.append(97);	freq.append(105000);
	ch.append(98);	freq.append(111000);
	ch.append(99);	freq.append(117000);
	ch.append(14);	freq.append(123000);
	ch.append(15);	freq.append(129000);
	ch.append(16);	freq.append(135000);
	ch.append(17);	freq.append(141000);
	ch.append(18);	freq.append(147000);
	ch.append(19);	freq.append(153000);
	ch.append(20);	freq.append(159000);
	ch.append(21);	freq.append(165000);
	ch.append(22);	freq.append(171000);
	ch.append(7);	freq.append(177000);
	ch.append(8);	freq.append(183000);
	ch.append(9);	freq.append(189000);
	ch.append(10);	freq.append(195000);
	ch.append(11);	freq.append(201000);
	ch.append(12);	freq.append(207000);
	ch.append(13);	freq.append(213000);
	ch.append(23);	freq.append(219000);
	ch.append(24);	freq.append(225000);
	ch.append(25);	freq.append(231000);
	ch.append(26);	freq.append(237000);
	ch.append(27);	freq.append(243000);
	ch.append(28);	freq.append(249000);
	ch.append(29);	freq.append(255000);
	ch.append(30);	freq.append(261000);
	ch.append(31);	freq.append(267000);
	ch.append(32);	freq.append(273000);
	ch.append(33);	freq.append(279000);
	ch.append(34);	freq.append(285000);
	ch.append(35);	freq.append(291000);
	ch.append(36);	freq.append(297000);
	ch.append(37);	freq.append(303000);
	ch.append(38);	freq.append(309000);
	ch.append(39);	freq.append(315000);
	ch.append(40);	freq.append(321000);
	ch.append(41);	freq.append(327000);
	ch.append(42);	freq.append(333000);
	ch.append(43);	freq.append(339000);
	ch.append(44);	freq.append(345000);
	ch.append(45);	freq.append(351000);
	ch.append(46);	freq.append(357000);
	ch.append(47);	freq.append(363000);
	ch.append(48);	freq.append(369000);
	ch.append(49);	freq.append(375000);
	ch.append(50);	freq.append(381000);
	ch.append(51);	freq.append(387000);
	ch.append(52);	freq.append(393000);
	ch.append(53);	freq.append(399000);
	ch.append(54);	freq.append(405000);
	ch.append(55);	freq.append(411000);
	ch.append(56);	freq.append(417000);
	ch.append(57);	freq.append(423000);
	ch.append(58);	freq.append(429000);
	ch.append(59);	freq.append(435000);
	ch.append(60);	freq.append(441000);
	ch.append(61);	freq.append(447000);
	ch.append(62);	freq.append(453000);
	ch.append(63);	freq.append(459000);
	ch.append(64);	freq.append(465000);
	ch.append(65);	freq.append(471000);
	ch.append(66);	freq.append(477000);
	ch.append(67);	freq.append(483000);
	ch.append(68);	freq.append(489000);
	ch.append(69);	freq.append(495000);
	ch.append(70);	freq.append(501000);
	ch.append(71);	freq.append(507000);
	ch.append(72);	freq.append(513000);
	ch.append(73);	freq.append(519000);
	ch.append(74);	freq.append(525000);
	ch.append(75);	freq.append(531000);
	ch.append(76);	freq.append(537000);
	ch.append(77);	freq.append(543000);
	ch.append(78);	freq.append(549000);
	ch.append(79);	freq.append(555000);
	ch.append(80);	freq.append(561000);
	ch.append(81);	freq.append(567000);
	ch.append(82);	freq.append(573000);
	ch.append(83);	freq.append(579000);
	ch.append(84);	freq.append(585000);
	ch.append(85);	freq.append(591000);
	ch.append(86);	freq.append(597000);
	ch.append(87);	freq.append(603000);
	ch.append(88);	freq.append(609000);
	ch.append(89);	freq.append(615000);
	ch.append(90);	freq.append(621000);
	ch.append(91);	freq.append(627000);
	ch.append(92);	freq.append(633000);
	ch.append(93);	freq.append(639000);
	ch.append(94);	freq.append(645000);
	ch.append(100);	freq.append(651000);
	ch.append(101);	freq.append(657000);
	ch.append(102);	freq.append(663000);
	ch.append(103);	freq.append(669000);
	ch.append(104);	freq.append(675000);
	ch.append(105);	freq.append(681000);
	ch.append(106);	freq.append(687000);
	ch.append(107);	freq.append(693000);
	ch.append(108);	freq.append(699000);
	ch.append(109);	freq.append(705000);
	ch.append(110);	freq.append(711000);
	ch.append(111);	freq.append(717000);
	ch.append(112);	freq.append(723000);
	ch.append(113);	freq.append(729000);
	ch.append(114);	freq.append(735000);
	ch.append(115);	freq.append(741000);
	ch.append(116);	freq.append(747000);
	ch.append(117);	freq.append(753000);
	ch.append(118);	freq.append(759000);
	ch.append(119);	freq.append(765000);
	ch.append(120);	freq.append(771000);
	ch.append(121);	freq.append(777000);
	ch.append(122);	freq.append(783000);
	ch.append(123);	freq.append(789000);
	ch.append(124);	freq.append(795000);
	ch.append(125);	freq.append(801000);
	ch.append(126);	freq.append(807000);
	ch.append(127);	freq.append(813000);
	ch.append(128);	freq.append(819000);
	ch.append(129);	freq.append(825000);
	ch.append(130);	freq.append(831000);
	ch.append(131);	freq.append(837000);
	ch.append(132);	freq.append(843000);
	ch.append(133);	freq.append(849000);
	ch.append(134);	freq.append(855000);
	ch.append(135);	freq.append(861000);
	ch.append(136);	freq.append(867000);
	ch.append(137);	freq.append(873000);
	ch.append(138);	freq.append(879000);
	ch.append(139);	freq.append(885000);
	ch.append(140);	freq.append(891000);
	ch.append(141);	freq.append(897000);
	ch.append(142);	freq.append(903000);
	ch.append(143);	freq.append(909000);
	ch.append(144);	freq.append(915000);
	ch.append(145);	freq.append(921000);
	ch.append(146);	freq.append(927000);
	ch.append(147);	freq.append(933000);
	ch.append(148);	freq.append(939000);
	ch.append(149);	freq.append(945000);
	ch.append(150);	freq.append(951000);
	ch.append(151);	freq.append(957000);
	ch.append(152);	freq.append(963000);
	ch.append(153);	freq.append(969000);
	ch.append(154);	freq.append(975000);
	ch.append(155);	freq.append(981000);
	ch.append(156);	freq.append(987000);
	ch.append(157);	freq.append(993000);
	ch.append(158);	freq.append(999000);
}

void freq_list::dvbt() {
	ch.clear();		freq.clear();
	ch.append(5);	freq.append(177500);
	ch.append(6);	freq.append(184500);
	ch.append(7);	freq.append(191500);
	ch.append(8);	freq.append(198500);
	ch.append(9);	freq.append(205500);
	ch.append(10);	freq.append(212500);
	ch.append(11);	freq.append(219500);
	ch.append(12);	freq.append(226500);
	ch.append(21);	freq.append(474000);
	ch.append(22);	freq.append(482000);
	ch.append(23);	freq.append(490000);
	ch.append(24);	freq.append(498000);
	ch.append(25);	freq.append(506000);
	ch.append(26);	freq.append(514000);
	ch.append(27);	freq.append(522000);
	ch.append(28);	freq.append(530000);
	ch.append(29);	freq.append(538000);
	ch.append(30);	freq.append(546000);
	ch.append(31);	freq.append(554000);
	ch.append(32);	freq.append(562000);
	ch.append(33);	freq.append(570000);
	ch.append(34);	freq.append(578000);
	ch.append(35);	freq.append(586000);
	ch.append(36);	freq.append(594000);
	ch.append(37);	freq.append(602000);
	ch.append(38);	freq.append(610000);
	ch.append(39);	freq.append(618000);
	ch.append(40);	freq.append(626000);
	ch.append(41);	freq.append(634000);
	ch.append(42);	freq.append(642000);
	ch.append(43);	freq.append(650000);
	ch.append(44);	freq.append(658000);
	ch.append(45);	freq.append(666000);
	ch.append(46);	freq.append(674000);
	ch.append(47);	freq.append(682000);
	ch.append(48);	freq.append(690000);
	ch.append(49);	freq.append(698000);
	ch.append(50);	freq.append(706000);
	ch.append(51);	freq.append(714000);
	ch.append(52);	freq.append(722000);
	ch.append(53);	freq.append(730000);
	ch.append(54);	freq.append(738000);
	ch.append(55);	freq.append(746000);
	ch.append(56);	freq.append(754000);
	ch.append(57);	freq.append(762000);
	ch.append(58);	freq.append(770000);
	ch.append(59);	freq.append(778000);
	ch.append(60);	freq.append(786000);
	ch.append(61);	freq.append(794000);
	ch.append(62);	freq.append(802000);
	ch.append(63);	freq.append(810000);
	ch.append(64);	freq.append(818000);
	ch.append(65);	freq.append(826000);
	ch.append(66);	freq.append(834000);
	ch.append(67);	freq.append(842000);
	ch.append(68);	freq.append(850000);
	ch.append(69);	freq.append(858000);
}
