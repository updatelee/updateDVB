/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HELPER_CLASSES_H
#define HELPER_CLASSES_H

#include <QString>
#include <QDebug>
#include <QPointer>
#include <QTreeWidget>
#include <QThread>
#include <QPointer>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>
#include <qwt_symbol.h>
#include <qwt_text.h>
#include <linux/dvb/frontend.h>
#include "classes/freq_list_class.h"

#define MAX_LNBS 16

#define GRAY QColor(80,80,80)
#define DGRAY QColor(60,60,60)
#define DGREEN QColor(0,100,0)
#define GREEN QColor(0,255,0)

#define TNY_BUFSIZE (20*188)
#define LIL_BUFSIZE (348*188)
#define BIG_BUFSIZE (30*LIL_BUFSIZE)
#define MAX_PES_SIZE (4*1024)
#define DMXOFF 5
#define PACKETRETRY 3
#define PERSISTENCE 30

// Status bits
#define TUNER_AVAIL		1  // free todo anything
#define TUNER_IOCTL		2  // busy sending ioctl
#define TUNER_IOCTL_QUEUE	4  // busy sending ioctl
#define TUNER_TUNED		8  // tuned
#define TUNER_DEMUX		16 // demux'ing
#define TUNER_RDING		32 // read()'ing
#define TUNER_SCAN		64 // busying scanning

#define STATUS_NOEXP   0
#define STATUS_REMOVE -1
#define STATUS_CLEAR  -2

const int KHz = 1000;
const int MHz = KHz * 1000;

class qwt_polarity
{
public:
	qwt_polarity();
	~qwt_polarity();

	void hide();
	void show();
	void set_color(QwtPlot *plot, QColor c);

	int f_start, f_stop, min, max;

	QwtPlotCurve* qwt_curve;
	QVector<QwtPlotMarker*> qwt_marker;
};

class qwt_data
{
public:
	qwt_data();
	~qwt_data();

	void hide();
	void show(int p);

	QVector<qwt_polarity*> qwt_pol;
};

struct asc1_data
{
	QString		name;
	unsigned int	counter;
	int		Hdeg;
	int		Vdeg;
};

class switch_settings
{
public:
	int voltage;
	int tone;
	int committed;
	int uncommitted;

	switch_settings();
};

class waitout : public QThread
{
public:
	waitout();
	void lock();
	void unlock();
	void wait();
	void wait(bool *loop);
private:
	bool ready;
};

class tp_info
{
public:
	tp_info();

	int frequency;
	int polarity;
	int symbolrate;
	int fec;
	int system;
	int modulation;
	int inversion;
	int rolloff;
	int pilot;
	int matype;
	int frame_len;
	unsigned int ucb;
	unsigned int ber;
	unsigned int ber_scale;
	float snr;
	unsigned int snr_scale;
	float lvl;
	unsigned int lvl_scale;
	int spectrumscan_lvl;
	int status;
};

class tuning_options : public tp_info
{
public:
	tuning_options();

	int f_start;
	int f_stop;
	int f_lof;
	int voltage;
	int tone;
	int committed;
	int uncommitted;
	int mis;
	unsigned int modcod;
	double site_lat, site_long;
	QString name;
};

class tree_item
{
public:
	tree_item();
	void save();
	void restore();

	int parent;
	int current;
	int saved;
	QString text;
	QColor color;
	bool expanded;
	bool return_parent;
	unsigned int pid;
	unsigned int table;
	QTreeWidgetItem * tree;
};

class dvb_pat
{
public:
	QVector<unsigned int> number;
	QVector<unsigned int> pid;
};

class dvb_pids
{
public:
	unsigned int pid;
	QVector<unsigned int> tbl;
	QVector<unsigned int> msk;
	unsigned int timeout;
};

class dvb_data
{
public:
	unsigned int pid;
	QVector<unsigned int> table;
	QVector<unsigned int> mask;
	QByteArray buffer;
};

class dvb_ca
{
public:
	QVector<unsigned int> system_id;
	QVector<unsigned int> pid;
};

class dvb_descriptor
{
public:
	QVector<unsigned int> id;
};

class dvb_pmt
{
public:
	unsigned int pcr;
	dvb_ca myca;
	unsigned int pmt_pid;
	unsigned int pmt_num;
	QVector<unsigned int> desc_type;
	QVector<unsigned int> desc_pid;
	QVector<dvb_descriptor> tag;
};

class dvb_sdt
{
public:
	QVector<unsigned int> sid;
	QVector<QString> sname;
	QVector<QString> pname;
};

bool isSatellite(int system);
bool isATSC(int system);
bool isVectorQAM(QVector<int> system);
bool isVectorATSC(QVector<int> system);
bool isVectorDVBT(QVector<int> system);
bool isQAM(int system);
bool isDVBT(int system);
int azero(int num);
QString tohex(unsigned long val, int length);
bool dvb_pids_containts(QVector<dvb_pids> vec, dvb_pids elm);
QString format_freq(int frequency, int system);

#endif // HELPER_CLASSES_H
