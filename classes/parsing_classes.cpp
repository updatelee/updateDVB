/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parsing_classes.h"

QString desc_data_class::whatis(unsigned long val)
{
	for (desc_data t : data) {
		if (val >= t.min && val <= t.max)
			return t.text;
	}
	return "unknown";
}

encoding_mode::encoding_mode()
{
	data.append({0x00, 0x00, "Unicode Code Range 0x0000 – 0x00FF"});
	data.append({0x01, 0x01, "Unicode Code Range 0x0100 – 0x01FF"});
	data.append({0x02, 0x02, "Unicode Code Range 0x0200 – 0x02FF"});
	data.append({0x03, 0x03, "Unicode Code Range 0x0300 – 0x03FF"});
	data.append({0x04, 0x04, "Unicode Code Range 0x0400 – 0x04FF"});
	data.append({0x05, 0x05, "Unicode Code Range 0x0500 – 0x05FF"});
	data.append({0x06, 0x06, "Unicode Code Range 0x0600 – 0x06FF"});
	data.append({0x07, 0x08, "Reserved"});
	data.append({0x09, 0x09, "Unicode Code Range 0x0900 – 0x09FF"});
	data.append({0x0A, 0x0A, "Unicode Code Range 0x0A00 – 0x0AFF"});
	data.append({0x0B, 0x0B, "Unicode Code Range 0x0B00 – 0x0BFF"});
	data.append({0x0C, 0x0C, "Unicode Code Range 0x0C00 – 0x0CFF"});
	data.append({0x0D, 0x0D, "Unicode Code Range 0x0D00 – 0x0DFF"});
	data.append({0x0E, 0x0E, "Unicode Code Range 0x0E00 – 0x0EFF"});
	data.append({0x0F, 0x0F, "Unicode Code Range 0x0F00 – 0x0FFF"});
	data.append({0x10, 0x10, "Unicode Code Range 0x1000 – 0x10FF"});
	data.append({0x11, 0x1F, "Reserved"});
	data.append({0x20, 0x20, "Unicode Code Range 0x2000 – 0x20FF"});
	data.append({0x21, 0x21, "Unicode Code Range 0x2100 – 0x21FF"});
	data.append({0x22, 0x22, "Unicode Code Range 0x2200 – 0x22FF"});
	data.append({0x23, 0x23, "Unicode Code Range 0x2300 – 0x23FF"});
	data.append({0x24, 0x24, "Unicode Code Range 0x2400 – 0x24FF"});
	data.append({0x25, 0x25, "Unicode Code Range 0x2500 – 0x25FF"});
	data.append({0x26, 0x26, "Unicode Code Range 0x2600 – 0x26FF"});
	data.append({0x27, 0x27, "Unicode Code Range 0x2700 – 0x27FF"});
	data.append({0x28, 0x2F, "Reserved"});
	data.append({0x30, 0x30, "Unicode Code Range 0x3000 – 0x30FF"});
	data.append({0x31, 0x31, "Unicode Code Range 0x3100 – 0x31FF"});
	data.append({0x32, 0x32, "Unicode Code Range 0x3200 – 0x32FF"});
	data.append({0x33, 0x33, "Unicode Code Range 0x3300 – 0x33FF"});
	data.append({0x34, 0x3D, "Reserved"});
	data.append({0x3E, 0x3E, "Standard Compression Scheme for Unicode (SCSU)"});
	data.append({0x3F, 0x3F, "Unicode, UTF-16 Form"});
	data.append({0x40, 0x41, "Assigned to ATSC standard for Taiwan"});
	data.append({0x42, 0x47, "Reserved for future ATSC use"});
	data.append({0x48, 0x48, "Assigned to ATSC standard for South Korea"});
	data.append({0x49, 0xDF, "Reserved for future ATSC use"});
	data.append({0xE0, 0xFE, "Used in other systems"});
	data.append({0xFF, 0xFF, "Not applicable"});
}

compression_type::compression_type()
{
	data.append({0x00, 0x00, "No compression"});
	data.append({0x01, 0x01, "Huffman coding using standard encode/decode tables defined in Table C4 and C5 in Annex C."});
	data.append({0x02, 0x02, "Huffman coding using standard encode/decode tables defined in Table C6 and C7 in Annex C."});
	data.append({0x03, 0xAF, "reserved"});
	data.append({0xB0, 0xFF, "Used in other systems"});
}

audio_type::audio_type()
{
	data.append({0x00, 0x00, "Undefined"});
	data.append({0x01, 0x01, "Clean effects"});
	data.append({0x02, 0x02, "Hearing impaired"});
	data.append({0x03, 0x03, "Visual impaired commentary"});
	data.append({0x04, 0x7F, "User Private"});
	data.append({0x80, 0xFF, "Reserved"});
}

cue_stream::cue_stream()
{
	data.append({0x00, 0x01, "splice_insert, splice_null, splice_schedule"});
	data.append({0x01, 0x02, "All Commands"});
	data.append({0x02, 0x02, "Segmentation"});
	data.append({0x03, 0x03, "Tiered Splicing"});
	data.append({0x04, 0x04, "Tiered Segmentation"});
	data.append({0x05, 0x7F, "Reserved"});
	data.append({0x80, 0xFF, "User Defined"});
}

atsc_service_type::atsc_service_type()
{
	data.append({0x00, 0x00, "[Reserved]"});
	data.append({0x01, 0x01, "Analog Television"});
	data.append({0x02, 0x02, "ATSC Digital Television"});
	data.append({0x03, 0x03, "ATSC Audio"});
	data.append({0x04, 0x04, "ATSC Data Only Service"});
	data.append({0x05, 0x05, "ATSC Software Download Service"});
	data.append({0x06, 0x3F, "[Reserved – see ATSC Code Points Registry3]"});
}

dvb_service_type::dvb_service_type()
{
	data.append({0x00, 0x00, "reserved for future use"});
	data.append({0x01, 0x01, "digital television service (see note 1)"});
	data.append({0x02, 0x02, "digital radio sound service (see note 2)"});
	data.append({0x03, 0x03, "Teletext service"});
	data.append({0x04, 0x04, "NVOD reference service (see note 1)"});
	data.append({0x05, 0x05, "NVOD time-shifted service (see note 1)"});
	data.append({0x06, 0x06, "mosaic service"});
	data.append({0x07, 0x07, "FM radio service"});
	data.append({0x08, 0x08, "DVB SRM service [48]"});
	data.append({0x09, 0x09, "reserved for future use"});
	data.append({0x0A, 0x0A, "advanced codec digital radio sound service"});
	data.append({0x0B, 0x0B, "advanced codec mosaic service"});
	data.append({0x0C, 0x0C, "data broadcast service"});
	data.append({0x0D, 0x0D, "reserved for Common Interface Usage (EN 50221 [37])"});
	data.append({0x0E, 0x0E, "RCS Map (see EN 301 790 [7])"});
	data.append({0x0F, 0x0F, "RCS FLS (see EN 301 790 [7])"});
	data.append({0x10, 0x10, "DVB MHP service"});
	data.append({0x11, 0x11, "MPEG-2 HD digital television service"});
	data.append({0x12, 0x15, "reserved for future use"});
	data.append({0x16, 0x16, "advanced codec SD digital television service"});
	data.append({0x17, 0x17, "advanced codec SD NVOD time-shifted service"});
	data.append({0x18, 0x18, "advanced codec SD NVOD reference service"});
	data.append({0x19, 0x19, "advanced codec HD digital television service"});
	data.append({0x1A, 0x1A, "advanced codec HD NVOD time-shifted service"});
	data.append({0x1B, 0x1B, "advanced codec HD NVOD reference service"});
	data.append({0x1C, 0x1C, "H.264/AVC frame compatible plano-stereoscopic HD digital television service (see note 3)"});
	data.append({0x1D, 0x1D, "H.264/AVC frame compatible plano-stereoscopic HD NVOD time-shifted service (see note 3)"});
	data.append({0x1E, 0x1E, "H.264/AVC frame compatible plano-stereoscopic HD NVOD reference service (see note 3)"});
	data.append({0x1F, 0x1F, "HEVC digital television service (see note 4)"});
	data.append({0x20, 0x7F, "reserved for future use"});
	data.append({0x80, 0xFE, "user defined"});
	data.append({0xFF, 0xFF, "reserved for future use"});
}

atsc_modulation::atsc_modulation()
{
	data.append({0x00, 0x00, "[Reserved]"});
	data.append({0x01, 0x01, "Analog — The virtual channel is modulated using standard analog methods for analog television"});
	data.append({0x02, 0x02, "SCTE_mode_1 — The virtual channel has a symbol rate of 5.057 Msps, transmitted in accordance with ANSI/SCTE 07 [21] (Mode 1). Typically, mode 1 will be used for 64-QAM."});
	data.append({0x03, 0x03, "SCTE_mode_2 — The virtual channel has a symbol rate of 5.361 Msps, transmitted in accordance with ANSI/SCTE 07 [21] (Mode 2). Typically, mode 2 will be used for 256-QAM."});
	data.append({0x04, 0x04, "ATSC (8 VSB) — The virtual channel uses the 8-VSB modulation method conforming to A/53 Part 2 [2]."});
	data.append({0x05, 0x05, "ATSC (16 VSB) — The virtual channel uses the 16-VSB modulation method conforming to A/53 Part 2 [2]."});
	data.append({0x06, 0x7F, "[Reserved for future use by ATSC]"});
	data.append({0x80, 0xFF, "[User Private]"});
}

qam_modulation::qam_modulation()
{
	data.append({0x00, 0x00, "not defined"});
	data.append({0x01, 0x01, "16-QAM"});
	data.append({0x02, 0x02, "32-QAM"});
	data.append({0x03, 0x03, "64-QAM"});
	data.append({0x04, 0x04, "128-QAM"});
	data.append({0x05, 0x05, "256-QAM"});
	data.append({0x06, 0xff, "reserved for future use"});
}

fec_outer::fec_outer()
{
	data.append({0x00, 0x00, "not defined"});
	data.append({0x01, 0x01, "no outer FEC coding"});
	data.append({0x02, 0x02, "RS(204/188)"});
	data.append({0x03, 0x0f, "reserved for future use"});
}

fec_inner::fec_inner()
{
	data.append({0x00, 0x00, "not defined"});
	data.append({0x01, 0x01, "1/2"});
	data.append({0x02, 0x02, "2/3"});
	data.append({0x03, 0x03, "3/4"});
	data.append({0x04, 0x04, "5/6"});
	data.append({0x05, 0x05, "7/8"});
	data.append({0x06, 0x06, "8/9"});
	data.append({0x07, 0x07, "3/5"});
	data.append({0x08, 0x08, "4/5"});
	data.append({0x09, 0x09, "9/10"});
	data.append({0x0a, 0x0e, "reserved for future use"});
	data.append({0x0f, 0x0f, "no conventional coding"});
}

mgt_table::mgt_table()
{
	data.append({0x0000, 0x0000, "Terrestrial VCT with current_next_indicator=1"});
	data.append({0x0001, 0x0001, "Terrestrial VCT with current_next_indicator=0"});
	data.append({0x0002, 0x0002, "Cable VCT with current_next_indicator=1"});
	data.append({0x0003, 0x0003, "Cable VCT with current_next_indicator=0"});
	data.append({0x0004, 0x0004, "Channel ETT"});
	data.append({0x0005, 0x0005, "DCCSCT"});
	data.append({0x0006, 0x0006, "LTST"});
	data.append({0x0007, 0x00FF, "(Reserved for future ATSC use)"});
	data.append({0x0010, 0x0010, "Short-form VCT – VCM Subtype"});
	data.append({0x0011, 0x0011, "Short-form VCT – DCM Subtype"});
	data.append({0x0012, 0x0012, "Short-form VCT – ICM Subtype"});
	data.append({0x0020, 0x0020, "Network Information Table - CDS Table Subtype"});
	data.append({0x0021, 0x0021, "Network Information Table - MMS Table Subtype"});
	data.append({0x0030, 0x0030, "Network Text Table – SNS Subtype"});
	data.append({0x0100, 0x017F, "EIT-0 to EIT-127"});
	data.append({0x0180, 0x01FF, "Reserved for future ATSC use"});
	data.append({0x0200, 0x027F, "Event ETT-0 to Event ETT-127"});
	data.append({0x0280, 0x0300, "Reserved for future ATSC use"});
	data.append({0x0301, 0x03FF, "RRT with rating_region 1-255"});
	data.append({0x0400, 0x0FFF, "User private"});
	data.append({0x1000, 0xFFFF, "Reserved for future ATSC use"});
	data.append({0x1000, 0x10FF, "Aggregate Extended Information Table with MGT_tag 0 to 255"});
	data.append({0x1100, 0x11FF, "SCTE Aggregate Extended Text Table with MGT_tag 0 to 255"});
	data.append({0x1180, 0x1180, "Long Term Service Table"});
	data.append({0x1200, 0x127F, "Extended Text Table for DET"});
	data.append({0x1300, 0x137F, "Data Event Table"});
	data.append({0x1380, 0x13FF, "Reserved for future ATSC use"});
	data.append({0x1400, 0x14FF, "DCCT (with dcc_id 0x00 – 0xFF)"});
	data.append({0x1500, 0x157F, "Aggregate Data Event Table"});
	data.append({0x1580, 0x15FF, "Reserved"});
	data.append({0x1600, 0x16FF, "Satellite VCT"});
}

data_broadcast_id::data_broadcast_id()
{
	data.append({0x0000, 0x0000, "Reserved for future use"});
	data.append({0x0001, 0x0001, "Data pipe"});
	data.append({0x0002, 0x0002, "Asynchronous data stream"});
	data.append({0x0003, 0x0003, "Synchronous data stream"});
	data.append({0x0004, 0x0004, "Synchronised data stream"});
	data.append({0x0005, 0x0005, "Multi protocol encapsulation"});
	data.append({0x0006, 0x0006, "Data Carousel"});
	data.append({0x0007, 0x0007, "Object Carousel"});
	data.append({0x0008, 0x0008, "DVB ATM streams"});
	data.append({0x0009, 0x0009, "Higher Protocols based on asynchronous data streams"});
	data.append({0x000A, 0x000A, "System Software Update"});
	data.append({0x000B, 0x00ef, "Reserved for future use by DVB"});
	data.append({0x00F0, 0x00F0, "MHP Object Carousel"});
	data.append({0x00F1, 0x00F1, "reserved for MHP Multi Protocol Encapsulation"});
	data.append({0x00F2, 0x00Fe, "Reserved for MHP use"});
	data.append({0x00FF, 0x00FF, "Reserved for future use by DVB"});
	data.append({0x0100, 0x0100, "Eutelsat Data Piping"});
	data.append({0x0101, 0x0101, "Eutelsat Data Streaming"});
	data.append({0x0102, 0x0102, "SAGEM IP encapsulation in MPEG-2 PES packets"});
	data.append({0x0103, 0x0103, "BARCO Data Broadcasting"});
	data.append({0x0104, 0x0104, "CyberCity Multiprotocol Encapsulation (New Media Communications Ltd.)"});
	data.append({0x0105, 0x0105, "CyberSat Multiprotocol Encapsulation (New Media Communications Ltd.)"});
	data.append({0x0106, 0x0106, "The Digital Network"});
	data.append({0x0107, 0x0107, "OpenTV Data Carousel"});
	data.append({0x0108, 0x0108, "Panasonic"});
	data.append({0x0109, 0x0109, "MSG MediaServices GmbH"});
	data.append({0x010A, 0x010A, "TechnoTrend"});
	data.append({0x010B, 0x010B, "Canal + Technologies system software download"});
	data.append({0x0110, 0x0110, "Televizja Polsat"});
	data.append({0x0111, 0x0111, "UK DTG"});
	data.append({0x0112, 0x0112, "SkyMedia"});
	data.append({0x0113, 0x0113, "Intellibyte DataBroadcasting"});
	data.append({0x0114, 0x0114, "TeleWeb Data Carousel"});
	data.append({0x0115, 0x0115, "TeleWeb Object Carousel"});
	data.append({0x0116, 0x0116, "TeleWeb"});
	data.append({0x4444, 0x4444, "4TV Data Broadcast"});
	data.append({0x4E4F, 0x4E4F, "Nokia IP based software delivery"});
	data.append({0xBBB1, 0xBBB1, "BBG Data Caroussel"});
	data.append({0xBBB2, 0xBBB2, "BBG Object Caroussel"});
	data.append({0xBBBB, 0xBBBB, "Bertelsmann Broadband Group"});
	data.append({0xFFFF, 0xFFFF, "Reserved for future use"});
}

transmission_context_id::transmission_context_id()
{
	data.append({0,   0,   "Transparent star TDMA, access dedicated to one RCST"});
	data.append({1,   1,   "Transparent star TDMA, slotted aloha"});
	data.append({2,   2,   "Transparent star TDMA, CRDSA"});
	data.append({3,   7,   "Reserved"});
	data.append({8,   8,   "Transparent star continuous transmission"});
	data.append({9,   15,  "Reserved"});
	data.append({16,  16,  "Transparent mesh overlay TDMA, general purpose"});
	data.append({17,  31,  "Reserved"});
	data.append({32,  32,  "Regenerative mesh TDMA, general purpose"});
	data.append({33,  127, "Reserved"});
	data.append({128, 255, "User Defined"});
}

private_data_specifier::private_data_specifier()
{
	data.append({0x00000000, 0x00000000, "Reserved"});
	data.append({0x00000000, 0x00000000, "Reserved"});
	data.append({0x00000001, 0x00000001, "SES"});
	data.append({0x00000002, 0x00000002, "BskyB 1"});
	data.append({0x00000003, 0x00000003, "BskyB 2"});
	data.append({0x00000004, 0x00000004, "BskyB 3"});
	data.append({0x00000005, 0x00000005, "ARD, ZDF, ORF"});
	data.append({0x00000006, 0x00000006, "Nokia Multimedia Network Terminals"});
	data.append({0x00000007, 0x00000007, "AT Entertainment Ltd."});
	data.append({0x00000008, 0x00000008, "TV Cabo Portugal"});
	data.append({0x00000009, 0x0000000D, "Nagravision SA // Kudelski"});
	data.append({0x0000000E, 0x0000000E, "Valvision SA"});
	data.append({0x0000000F, 0x0000000F, "Quiero Television"});
	data.append({0x00000010, 0x00000010, "La Television Par Satellite (TPS)"});
	data.append({0x00000011, 0x00000011, "Echostar Communications"});
	data.append({0x00000012, 0x00000012, "Telia AB"});
	data.append({0x00000013, 0x00000013, "Viasat"});
	data.append({0x00000014, 0x00000014, "Senda (Swedish Terrestrial TV)"});
	data.append({0x00000015, 0x00000015, "MediaKabel"});
	data.append({0x00000016, 0x00000016, "Casema"});
	data.append({0x00000017, 0x00000017, "Humax Electronics Co. Ltd."});
	data.append({0x00000018, 0x00000018, "@Sky"});
	data.append({0x00000019, 0x00000019, "Singapore Digital Terrestrial Television"});
	data.append({0x0000001A, 0x0000001A, "Telediffusion de France (TDF)"});
	data.append({0x0000001B, 0x0000001B, "Intellibyte Inc."});
	data.append({0x0000001C, 0x0000001C, "Digital Theater Systems Ltd"});
	data.append({0x0000001D, 0x0000001D, "Finlux Ltd."});
	data.append({0x0000001E, 0x0000001E, "Sagem SA"});
	data.append({0x00000020, 0x00000023, "Lyonnaise Cable"});
	data.append({0x00000025, 0x00000025, "MTV Europe"});
	data.append({0x00000026, 0x00000026, "Pansonic"});
	data.append({0x00000027, 0x00000027, "Mentor Data System, Inc."});
	data.append({0x00000028, 0x00000028, "EACEM"});
	data.append({0x00000029, 0x00000029, "NorDig"});
	data.append({0x0000002A, 0x0000002A, "Intelsis Sistemas Inteligentes S.A."});
	data.append({0x0000002D, 0x0000002D, "Alpha Digital Synthesis S.A."});
	data.append({0x0000002F, 0x0000002F, "Conax A.S."});
	data.append({0x00000030, 0x00000030, "Telenor"});
	data.append({0x00000031, 0x00000031, "TeleDenmark"});
	data.append({0x00000035, 0x00000035, "Europe Online Networks S.A."});
	data.append({0x00000038, 0x00000038, "OTE"});
	data.append({0x00000039, 0x00000039, "Telewizja Polsat"});
	data.append({0x000000A0, 0x000000A0, "Sentech"});
	data.append({0x000000A1, 0x000000A1, "TechniSat Digital GmbH"});
	data.append({0x000000BE, 0x000000BE, "BetaTechnik"});
	data.append({0x000000C0, 0x000000C0, "Canal+"});
	data.append({0x000000D0, 0x000000D0, "Dolby Laboratories Inc."});
	data.append({0x000000E0, 0x000000E0, "ExpressVu Inc."});
	data.append({0x000000F0, 0x000000F0, "France Telecom, CNES and DGA (STENTOR)"});
	data.append({0x00000100, 0x00000100, "OpenTV"});
	data.append({0x00000150, 0x00000150, "Loewe Opta GmbH"});
	data.append({0x00000600, 0x00000601, "UPC 1"});
	data.append({0x00000ACE, 0x00000ACE, "Ortikon Interactive Oy"});
	data.append({0x00001000, 0x00001000, "La Television Par Satellite (TPS)"});
	data.append({0x000022D4, 0x000022D4, "Spanish Broadcasting Regulator"});
	data.append({0x000022F1, 0x000022F1, "Swedish Broadcasting Regulator"});
	data.append({0x0000233A, 0x0000233A, "Independent Television Commission"});
	data.append({0x00003200, 0x0000320f, "Australian Terrestrial Television Networks"});
	data.append({0x00006000, 0x00006000, "News Datacom"});
	data.append({0x00006001, 0x00006006, "NDC"});
	data.append({0x00362275, 0x00362275, "Irdeto"});
	data.append({0x004E544C, 0x004E544C, "NTL"});
	data.append({0x00532D41, 0x00532D41, "Scientific Atlanta"});
	data.append({0x00600000, 0x00600000, "Rhene Vision Cable"});
	data.append({0x44414E59, 0x44414E59, "News Datacom (IL) 1"});
	data.append({0x46524549, 0x46524549, "News Datacom (IL) 1"});
	data.append({0x46545600, 0x46545620, "FreeTV"});
	data.append({0x4A4F4A4F, 0x4A4F4A4F, "MSG MediaServices GmbH"});
	data.append({0x4F545600, 0x4F5456ff, "OpenTV"});
	data.append({0x50484900, 0x504849ff, "Philips DVS"});
	data.append({0x53415053, 0x53415053, "Scientific Atlanta"});
	data.append({0x5347444E, 0x5347444E, "StarGuide Digital Networks"});
	data.append({0x56444700, 0x56444700, "Via Digital"});
	data.append({0xBBBBBBBB, 0xBBBBBBBB, "Bertelsmann Broadband Group"});
	data.append({0xECCA0001, 0xECCA0001, "ECCA (European Cable Communications Association)"});
	data.append({0xFCFCFCFC, 0xFCFCFCFC, "France Telecom"});
}

QString stream_content::whatis(unsigned int stream_type, unsigned int component_type)
{
	for (int i = 0; i < text.size(); i++) {
		if (stream_type == sval.at(i) && component_type >= cmin.at(i) && component_type <= cmax.at(i)) {
			return text.at(i);
		}
	}
	return "user defined";
}

stream_content::stream_content()
{
	sval.append(0x00);	cmin.append(0x00);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x01);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x01);	cmin.append(0x01);	cmax.append(0x01);	text.append("MPEG-2 video, 4:3 aspect ratio, 25 Hz");
	sval.append(0x01);	cmin.append(0x02);	cmax.append(0x02);	text.append("MPEG-2 video, 16:9 aspect ratio with pan vectors, 25 Hz");
	sval.append(0x01);	cmin.append(0x03);	cmax.append(0x03);	text.append("MPEG-2 video, 16:9 aspect ratio without pan vectors, 25 Hz");
	sval.append(0x01);	cmin.append(0x04);	cmax.append(0x04);	text.append("MPEG-2 video, > 16:9 aspect ratio, 25 Hz");
	sval.append(0x01);	cmin.append(0x05);	cmax.append(0x05);	text.append("MPEG-2 video, 4:3 aspect ratio, 30 Hz");
	sval.append(0x01);	cmin.append(0x06);	cmax.append(0x06);	text.append("MPEG-2 video, 16:9 aspect ratio with pan vectors, 30 Hz");
	sval.append(0x01);	cmin.append(0x07);	cmax.append(0x07);	text.append("MPEG-2 video, 16:9 aspect ratio without pan vectors, 30 Hz");
	sval.append(0x01);	cmin.append(0x08);	cmax.append(0x08);	text.append("MPEG-2 video, > 16:9 aspect ratio, 30 Hz");
	sval.append(0x01);	cmin.append(0x09);	cmax.append(0x09);	text.append("MPEG-2 high definition video, 4:3 aspect ratio, 25 Hz");
	sval.append(0x01);	cmin.append(0x0A);	cmax.append(0x0A);	text.append("MPEG-2 high definition video, 16:9 aspect ratio with pan vectors, 25 Hz");
	sval.append(0x01);	cmin.append(0x0B);	cmax.append(0x0B);	text.append("MPEG-2 high definition video, 16:9 aspect ratio without pan vectors, 25 Hz");
	sval.append(0x01);	cmin.append(0x0C);	cmax.append(0x0C);	text.append("MPEG-2 high definition video, > 16:9 aspect ratio, 25 Hz");
	sval.append(0x01);	cmin.append(0x0D);	cmax.append(0x0D);	text.append("MPEG-2 high definition video, 4:3 aspect ratio, 30 Hz");
	sval.append(0x01);	cmin.append(0x0E);	cmax.append(0x0E);	text.append("MPEG-2 high definition video, 16:9 aspect ratio with pan vectors, 30 Hz");
	sval.append(0x01);	cmin.append(0x0F);	cmax.append(0x0F);	text.append("MPEG-2 high definition video, 16:9 aspect ratio without pan vectors, 30 Hz");
	sval.append(0x01);	cmin.append(0x10);	cmax.append(0x10);	text.append("MPEG-2 high definition video, > 16:9 aspect ratio, 30 Hz");
	sval.append(0x01);	cmin.append(0x11);	cmax.append(0xAF);	text.append("reserved for future use");
	sval.append(0x01);	cmin.append(0xB0);	cmax.append(0xFE);	text.append("user defined");
	sval.append(0x01);	cmin.append(0xFF);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x02);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x02);	cmin.append(0x01);	cmax.append(0x01);	text.append("MPEG-1 Layer 2 audio, single mono channel");
	sval.append(0x02);	cmin.append(0x02);	cmax.append(0x02);	text.append("MPEG-1 Layer 2 audio, dual mono channel");
	sval.append(0x02);	cmin.append(0x03);	cmax.append(0x03);	text.append("MPEG-1 Layer 2 audio, stereo (2 channel)");
	sval.append(0x02);	cmin.append(0x04);	cmax.append(0x04);	text.append("MPEG-1 Layer 2 audio, multi-lingual, multi-channel");
	sval.append(0x02);	cmin.append(0x05);	cmax.append(0x05);	text.append("MPEG-1 Layer 2 audio, surround sound");
	sval.append(0x02);	cmin.append(0x06);	cmax.append(0x3F);	text.append("reserved for future use");
	sval.append(0x02);	cmin.append(0x40);	cmax.append(0x40);	text.append("MPEG-1 Layer 2 audio description for the visually impaired");
	sval.append(0x02);	cmin.append(0x41);	cmax.append(0x41);	text.append("MPEG-1 Layer 2 audio for the hard of hearing");
	sval.append(0x02);	cmin.append(0x42);	cmax.append(0x42);	text.append("receiver-mixed supplementary audio as per annex E of TS 101 154 [9]");
	sval.append(0x02);	cmin.append(0x43);	cmax.append(0x46);	text.append("reserved for future use");
	sval.append(0x02);	cmin.append(0x47);	cmax.append(0x47);	text.append("MPEG-1 Layer 2 audio, receiver mix audio description as per annex E of");
	sval.append(0x02);	cmin.append(0x48);	cmax.append(0x48);	text.append("MPEG-1 Layer 2 audio, broadcaster mix audio description");
	sval.append(0x02);	cmin.append(0x49);	cmax.append(0xAF);	text.append("reserved for future use");
	sval.append(0x02);	cmin.append(0xB0);	cmax.append(0xFE);	text.append("user-defined");
	sval.append(0x02);	cmin.append(0xFF);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x01);	cmax.append(0x01);	text.append("EBU Teletext subtitles");
	sval.append(0x03);	cmin.append(0x02);	cmax.append(0x02);	text.append("associated EBU Teletext");
	sval.append(0x03);	cmin.append(0x03);	cmax.append(0x03);	text.append("VBI data");
	sval.append(0x03);	cmin.append(0x04);	cmax.append(0x0F);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x10);	cmax.append(0x10);	text.append("DVB subtitles (normal) with no monitor aspect ratio criticality");
	sval.append(0x03);	cmin.append(0x11);	cmax.append(0x11);	text.append("DVB subtitles (normal) for display on 4:3 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x12);	cmax.append(0x12);	text.append("DVB subtitles (normal) for display on 16:9 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x13);	cmax.append(0x13);	text.append("DVB subtitles (normal) for display on 2.21:1 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x14);	cmax.append(0x14);	text.append("DVB subtitles (normal) for display on a high definition monitor");
	sval.append(0x03);	cmin.append(0x15);	cmax.append(0x1F);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x20);	cmax.append(0x20);	text.append("DVB subtitles (for the hard of hearing) with no monitor aspect ratio criticality");
	sval.append(0x03);	cmin.append(0x21);	cmax.append(0x21);	text.append("DVB subtitles (for the hard of hearing) for display on 4:3 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x22);	cmax.append(0x22);	text.append("DVB subtitles (for the hard of hearing) for display on 16:9 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x23);	cmax.append(0x23);	text.append("DVB subtitles (for the hard of hearing) for display on 2.21:1 aspect ratio monitor");
	sval.append(0x03);	cmin.append(0x24);	cmax.append(0x24);	text.append("DVB subtitles (for the hard of hearing) for display on a high definition monitor");
	sval.append(0x03);	cmin.append(0x25);	cmax.append(0x2F);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x30);	cmax.append(0x30);	text.append("Open (in-vision) sign language interpretation for the deaf");
	sval.append(0x03);	cmin.append(0x31);	cmax.append(0x31);	text.append("Closed sign language interpretation for the deaf");
	sval.append(0x03);	cmin.append(0x32);	cmax.append(0x3F);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0x40);	cmax.append(0x40);	text.append("video up-sampled from standard definition source material");
	sval.append(0x03);	cmin.append(0x41);	cmax.append(0xAF);	text.append("reserved for future use");
	sval.append(0x03);	cmin.append(0xB0);	cmax.append(0xFE);	text.append("user defined");
	sval.append(0x03);	cmin.append(0xFF);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x04);	cmin.append(0x00);	cmax.append(0x7F);	text.append("reserved for AC-3 audio modes (refer to table D.1)");
	sval.append(0x04);	cmin.append(0x80);	cmax.append(0xFF);	text.append("reserved for enhanced AC-3 audio modes (refer to table D.1)");
	sval.append(0x05);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0x01);	cmax.append(0x01);	text.append("H.264/AVC standard definition video, 4:3 aspect ratio, 25 Hz");
	sval.append(0x05);	cmin.append(0x02);	cmax.append(0x02);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0x03);	cmax.append(0x03);	text.append("H.264/AVC standard definition video, 16:9 aspect ratio, 25 Hz");
	sval.append(0x05);	cmin.append(0x04);	cmax.append(0x04);	text.append("H.264/AVC standard definition video, > 16:9 aspect ratio, 25 Hz");
	sval.append(0x05);	cmin.append(0x05);	cmax.append(0x05);	text.append("H.264/AVC standard definition video, 4:3 aspect ratio, 30 Hz");
	sval.append(0x05);	cmin.append(0x06);	cmax.append(0x06);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0x07);	cmax.append(0x07);	text.append("H.264/AVC standard definition video, 16:9 aspect ratio, 30 Hz");
	sval.append(0x05);	cmin.append(0x08);	cmax.append(0x08);	text.append("H.264/AVC standard definition video, > 16:9 aspect ratio, 30 Hz");
	sval.append(0x05);	cmin.append(0x09);	cmax.append(0x0A);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0x0B);	cmax.append(0x0B);	text.append("H.264/AVC high definition video, 16:9 aspect ratio, 25 Hz");
	sval.append(0x05);	cmin.append(0x0C);	cmax.append(0x0C);	text.append("H.264/AVC high definition video, > 16:9 aspect ratio, 25 Hz");
	sval.append(0x05);	cmin.append(0x0D);	cmax.append(0x0E);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0x0F);	cmax.append(0x0F);	text.append("H.264/AVC high definition video, 16:9 aspect ratio, 30 Hz");
	sval.append(0x05);	cmin.append(0x10);	cmax.append(0x10);	text.append("H.264/AVC high definition video, > 16:9 aspect ratio, 30 Hz");
	sval.append(0x05);	cmin.append(0x11);	cmax.append(0xAF);	text.append("reserved for future use");
	sval.append(0x05);	cmin.append(0xB0);	cmax.append(0xFE);	text.append("user-defined");
	sval.append(0x05);	cmin.append(0xFF);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0x01);	cmax.append(0x01);	text.append("HE-AAC audio, single mono channel");
	sval.append(0x06);	cmin.append(0x02);	cmax.append(0x02);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0x03);	cmax.append(0x03);	text.append("HE-AAC audio, stereo");
	sval.append(0x06);	cmin.append(0x04);	cmax.append(0x04);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0x05);	cmax.append(0x05);	text.append("HE-AAC audio, surround sound");
	sval.append(0x06);	cmin.append(0x06);	cmax.append(0x3F);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0x40);	cmax.append(0x40);	text.append("HE-AAC audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x41);	cmax.append(0x41);	text.append("HE-AAC audio for the hard of hearing");
	sval.append(0x06);	cmin.append(0x42);	cmax.append(0x42);	text.append("HE-AAC receiver-mixed supplementary audio as per annex E of TS 101 154 [9]");
	sval.append(0x06);	cmin.append(0x43);	cmax.append(0x43);	text.append("HE-AAC v2 audio, stereo");
	sval.append(0x06);	cmin.append(0x44);	cmax.append(0x44);	text.append("HE-AAC v2 audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x45);	cmax.append(0x45);	text.append("HE-AAC v2 audio for the hard of hearing");
	sval.append(0x06);	cmin.append(0x46);	cmax.append(0x46);	text.append("HE-AAC v2 receiver-mixed supplementary audio as per annex E of TS 101 154 [9]");
	sval.append(0x06);	cmin.append(0x47);	cmax.append(0x47);	text.append("HE-AAC receiver mix audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x48);	cmax.append(0x48);	text.append("HE-AAC broadcaster mix audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x49);	cmax.append(0x49);	text.append("HE-AAC v2 receiver mix audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x4A);	cmax.append(0x4A);	text.append("HE-AAC v2 broadcaster mix audio description for the visually impaired");
	sval.append(0x06);	cmin.append(0x4B);	cmax.append(0xAF);	text.append("reserved for future use");
	sval.append(0x06);	cmin.append(0xB0);	cmax.append(0xFE);	text.append("user-defined");
	sval.append(0x06);	cmin.append(0xFF);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x07);	cmin.append(0x00);	cmax.append(0x7F);	text.append("reserved for DTS audio modes (refer to annex G)");
	sval.append(0x07);	cmin.append(0x80);	cmax.append(0xFF);	text.append("reserved for future use");
	sval.append(0x08);	cmin.append(0x00);	cmax.append(0x00);	text.append("reserved for future use");
	sval.append(0x08);	cmin.append(0x01);	cmax.append(0x01);	text.append("DVB SRM data [48]");
	sval.append(0x08);	cmin.append(0x02);	cmax.append(0xFF);	text.append("reserved for DVB CPCM modes [46] to [i.4]");
}

data_service::data_service()
{
	text.fill("reserved for future use", 0xFF);
	text[0x00] = "reserved for future use";
	text[0x01] = "EBU teletext (Requires additional teletext_descriptor)";
	text[0x02] = "inverted teletext";
	text[0x03] = "reserved";
	text[0x04] = "VPS";
	text[0x05] = "WSS";
	text[0x06] = "Closed Captioning";
	text[0x07] = "monochrome 4:2:2 samples";
}

rating_region::rating_region()
{
	name.append("Undefined");
	name.append("US");
	name.append("Canada");
	name.append("Taiwan");
	name.append("South Korea");
	name.append("Alternate US Rating Region");
}

ac3_desc::ac3_desc()
{
	sample_rate_code.append("48 khz");
	sample_rate_code.append("44.1 khz");
	sample_rate_code.append("32 khz");
	sample_rate_code.append("Reserved");
	sample_rate_code.append("48 or 44.1khz");
	sample_rate_code.append("48 or 32khz");
	sample_rate_code.append("44.1 or 32khz");
	sample_rate_code.append("48 or 44.1 or 32khz");

	bit_rate_code.fill("Unknown", 0xFF);
	bit_rate_code[0] = "32 kbit/sec";
	bit_rate_code[1] = "40 kbit/sec";
	bit_rate_code[2] = "48 kbit/sec";
	bit_rate_code[3] = "56 kbit/sec";
	bit_rate_code[4] = "64 kbit/sec";
	bit_rate_code[5] = "80 kbit/sec";
	bit_rate_code[6] = "96 kbit/sec";
	bit_rate_code[7] = "112 kbit/sec";
	bit_rate_code[8] = "128 kbit/sec";
	bit_rate_code[9] = "160 kbit/sec";
	bit_rate_code[10] = "192 kbit/sec";
	bit_rate_code[11] = "224 kbit/sec";
	bit_rate_code[12] = "256 kbit/sec";
	bit_rate_code[13] = "320 kbit/sec";
	bit_rate_code[14] = "384 kbit/sec";
	bit_rate_code[15] = "448 kbit/sec";
	bit_rate_code[16] = "512 kbit/sec";
	bit_rate_code[17] = "576 kbit/sec";
	bit_rate_code[18] = "640 kbit/sec";

	dsurmod.append("Not Indicated");
	dsurmod.append("Not Dolby Suround Encoded");
	dsurmod.append("Dolby Suround Encoded");
	dsurmod.append("Reserved");

	num_channels.append("1+1");
	num_channels.append("1/0");
	num_channels.append("2/0");
	num_channels.append("3/0");
	num_channels.append("2/1");
	num_channels.append("3/1");
	num_channels.append("2/2");
	num_channels.append("3/2");
	num_channels.append("1");
	num_channels.append("<=2");
	num_channels.append("<=3");
	num_channels.append("<=4");
	num_channels.append("<=5");
	num_channels.append("<=6");
	num_channels.append("Reserved");
	num_channels.append("Reserved");

	priority.append("Reserved");
	priority.append("Primary Audio");
	priority.append("Other Audio");
	priority.append("Not Specified");

	bsmode.append("main audio service - complete main (CM)");
	bsmode.append("main audio service - music & effects (ME)");
	bsmode.append("associated service - visually impaired (VI)");
	bsmode.append("associated service - hearing impaired (HI)");
	bsmode.append("associated service - dialogue (D)");
	bsmode.append("associated service - commentary (C)");
	bsmode.append("associated service - emergency (E)");
	bsmode.append("associated service - voice over (VO)");
}

data_stream_type::data_stream_type()
{
	name.fill("Reserved", 0xFF);
	name[0x01] = "Slice or Video Access Unit";
	name[0x02] = "Video Access Unit";
	name[0x03] = "GOP or SEQ";
	name[0x04] = "SEQ";
}

AAC::AAC()
{
	profile.fill("Reserved", 0xFF);
	profile[0x00] = "Main";
	profile[0x01] = "Low Complexity";
	profile[0x02] = "Scalable Sampling Rate";

	channel_configuration.fill("Reserved", 0xFF);
	channel_configuration[0x00] = "Invalid";
	channel_configuration[0x01] = "center front speaker";
	channel_configuration[0x02] = "left, right front speakers";
	channel_configuration[0x03] = "center front speaker, left, right front speakers";
	channel_configuration[0x04] = "center front speaker, left, right center front speakers, rear surround";
	channel_configuration[0x05] = "center front speaker, left, right front speakers, left surround, right surround, rear speakers";
	channel_configuration[0x06] = "center front speaker, left, right front speakers, left surround, right surround, rear speakers, front low frequency effects speaker";
	channel_configuration[0x07] = "center front speaker, left, right center front speakers, left, right outside front speakers, left surround, right surround, rear speakers, front low frequency effects speaker";
}

frame_rate::frame_rate()
{
	rate.fill("Reserved", 0xFF);
	rate[0x00] = "Forbidden";
	rate[0x01] = "23.976 fps";
	rate[0x02] = "24 fps";
	rate[0x03] = "25 fps";
	rate[0x04] = "29.97 fps";
	rate[0x05] = "30 fps";
	rate[0x06] = "50 fps";
	rate[0x07] = "59.95 fps";
	rate[0x08] = "60 fps";
}

etm_location::etm_location()
{
	text.append("No ETM");
	text.append("ETM located in the PTC carrying this PSIP");
	text.append("ETM located in the PTC specified by the channel_TSID");
	text.append("[Reserved for future ATSC use]");
}

frame_rate_code::frame_rate_code()
{
	text.fill("Reserved", 0x0F);
	text[0x00] = "forbidden";
	text[0x01] = "23.976";
	text[0x02] = "24.000";
	text[0x03] = "25.000";
	text[0x04] = "29.97";
	text[0x05] = "30.000";
	text[0x06] = "50.000";
	text[0x07] = "59.94";
	text[0x08] = "60.000";
}

teletext_type::teletext_type()
{
	text.fill("Reserved", 0x1F);
	text[0x01] = "initial Teletext pagev";
	text[0x02] = "Teletext subtitle page";
	text[0x03] = "additional information page";
	text[0x04] = "programme schedule page";
	text[0x05] = "Teletext subtitle page for hearing impaired people";
}

coding_type::coding_type()
{
	text.append("Not Defined");
	text.append("Satellite");
	text.append("Cable");
	text.append("Terrestrial");
}

t_delsys_bandwidth::t_delsys_bandwidth()
{
	text.append("8 MHz");
	text.append("7 MHz");
	text.append("6 MHz");
	text.append("5 MHz");
	text.append("reserved");
}

t_delsys_constellation::t_delsys_constellation()
{
	text.append("QPSK");
	text.append("QAM 16");
	text.append("QAM 64");
	text.append("reserved");
}

t_delsys_hierarchy_information::t_delsys_hierarchy_information()
{
	text.append("non-hierarchical, native interleaver");
	text.append("α = 1, native interleaver");
	text.append("α = 2, native interleaver");
	text.append("α = 4, native interleaver");
	text.append("non-hierarchical, in-depth interleaver");
	text.append("α = 1, in-depth interleaver");
	text.append("α = 2, in-depth interleaver");
	text.append("α = 4, in-depth interleaver ");
}

t_delsys_fec::t_delsys_fec()
{
	text.append("1/2");
	text.append("2/3");
	text.append("3/4");
	text.append("5/6");
	text.append("7/8");
	text.append("reserved");
	text.append("reserved");
	text.append("reserved");
}

t_delsys_guard::t_delsys_guard()
{
	text.append("1/32");
	text.append("1/16");
	text.append("1/8");
	text.append("1/4");
}

t_delsys_tmode::t_delsys_tmode()
{
	text.append("2k");
	text.append("8k");
	text.append("4k");
	text.append("reserved");
}
