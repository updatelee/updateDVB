/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "master_class.h"

master_class::master_class()
{
	mysettings = new QSettings("UDL", "updateDVB");
	reload_settings();
}

master_class::~master_class()
{
}

void master_class::reload_settings()
{
	mysettings->sync();
	tune_ops.clear();

	tuning_options tmp;
	for (int a = 0; a < MAX_LNBS; a++) {
		tmp.f_lof	= mysettings->value("lnb"+QString::number(a)+"_freqlof").toInt();
		tmp.f_start	= mysettings->value("lnb"+QString::number(a)+"_freqstart").toInt();
		tmp.f_stop	= mysettings->value("lnb"+QString::number(a)+"_freqstop").toInt();
		tmp.voltage	= mysettings->value("lnb"+QString::number(a)+"_voltage").toInt();
		tmp.tone	= mysettings->value("lnb"+QString::number(a)+"_tone").toBool();
		tmp.mis		= -1;
		tmp.committed	= mysettings->value("lnb"+QString::number(a)+"_committed").toInt();
		tmp.uncommitted	= mysettings->value("lnb"+QString::number(a)+"_uncommitted").toInt();
		tmp.site_lat	= mysettings->value("site_lat").toDouble();
		tmp.site_long	= mysettings->value("site_long").toDouble();
		tmp.name	= mysettings->value("name").toString();
		tmp.modcod	= 0x0fffffff;
		tune_ops.append(tmp);
	}
}
