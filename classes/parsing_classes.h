/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSING_CLASSES_H
#define PARSING_CLASSES_H

#include <QString>
#include <QVector>

struct desc_data
{
	unsigned long min;
	unsigned long max;
	QString text;
};

class desc_data_class
{
public:
	QString whatis(unsigned long val);
	QVector<desc_data> data;
};

class encoding_mode : public desc_data_class
{
public:
	encoding_mode();
};

class compression_type : public desc_data_class
{
public:
	compression_type();
};

class audio_type : public desc_data_class
{
public:
	audio_type();
};

class cue_stream : public desc_data_class
{
public:
	cue_stream();
};

class atsc_service_type : public desc_data_class
{
public:
	atsc_service_type();
};

class dvb_service_type : public desc_data_class
{
public:
	dvb_service_type();
};

class atsc_modulation : public desc_data_class
{
public:
	atsc_modulation();
};

class qam_modulation : public desc_data_class
{
public:
	qam_modulation();
};

class fec_outer : public desc_data_class
{
public:
	fec_outer();
};

class fec_inner : public desc_data_class
{
public:
	fec_inner();
};

class mgt_table : public desc_data_class
{
public:
	mgt_table();
};

class data_broadcast_id : public desc_data_class
{
public:
	data_broadcast_id();
};

class transmission_context_id : public desc_data_class
{
public:
	transmission_context_id();
};

class private_data_specifier : public desc_data_class
{
public:
	private_data_specifier();
};

class stream_content
{
public:
	stream_content();
	QString whatis(unsigned int stream_type, unsigned int component_type);
	QVector<QString> text;
	QVector<unsigned int> sval;
	QVector<unsigned int> cmin;
	QVector<unsigned int> cmax;
};

class data_service
{
public:
	data_service();

	QVector<QString> text;
};

class rating_region
{
public:
	rating_region();
	QVector<QString> name;
};

class ac3_desc
{
public:
	ac3_desc();
	QVector<QString> sample_rate_code;
	QVector<QString> bit_rate_code;
	QVector<QString> dsurmod;
	QVector<QString> num_channels;
	QVector<QString> priority;
	QVector<QString> bsmode;
};

class data_stream_type
{
public:
	data_stream_type();
	QVector<QString> name;
};

class AAC
{
public:
	AAC();
	QVector<QString> profile;
	QVector<QString> channel_configuration;
};

class frame_rate
{
public:
	frame_rate();
	QVector<QString> rate;
};

class frame_rate_code
{
public:
	frame_rate_code();

	QVector<QString> text;
};

class etm_location
{
public:
	etm_location();

	QVector<QString> text;
};

class teletext_type
{
public:
	teletext_type();

	QVector<QString> text;
};

class coding_type
{
public:
	coding_type();

	QVector<QString> text;
};

class t_delsys_bandwidth
{
public:
	t_delsys_bandwidth();

	QVector<QString> text;
};

class t_delsys_constellation
{
public:
	t_delsys_constellation();

	QVector<QString> text;
};

class t_delsys_hierarchy_information
{
public:
	t_delsys_hierarchy_information();

	QVector<QString> text;
};

class t_delsys_fec
{
public:
	t_delsys_fec();

	QVector<QString> text;
};

class t_delsys_guard
{
public:
	t_delsys_guard();

	QVector<QString> text;
};

class t_delsys_tmode
{
public:
	t_delsys_tmode();

	QVector<QString> text;
};

#endif // PARSING_CLASSES_H
