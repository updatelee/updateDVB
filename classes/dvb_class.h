/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DVB_CLASS_H
#define DVB_CLASS_H

#include <QtCore>
#include <QObject>
#include <qwt_plot.h>
#include "classes/master_class.h"
#include "threads/dvbtune_thread.h"

class dvb_class : public QObject
{
	Q_OBJECT
public:
	dvb_class(master_class *m);
	~dvb_class();

	master_class *mc;
	QwtPlot *qwt_plot;
	dvbtune_thread *dvb_thread;

public slots:
	void emit_update_status(QString text, int time);

signals:
	void update_status(QString text, int time);
};

#endif // DVB_CLASS_H
