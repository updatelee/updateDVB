/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTRUMSCAN_TAB_WORKER_H
#define SPECTRUMSCAN_TAB_WORKER_H

#include <QtCore>
#include <QObject>
#include <QElapsedTimer>
#include <QPen>
#include <qwt_plot.h>
#include <qwt_legend.h>
#include <qwt_plot_picker.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>
#include <qwt_picker_machine.h>
#include "classes/dvb_class.h"
#include "classes/master_class.h"
#include "threads/spectrumscan_thread.h"

class spectrumscan_tab_worker : public QObject
{
	Q_OBJECT
public:
	explicit spectrumscan_tab_worker(dvb_class *d);
	~spectrumscan_tab_worker();

	void curve_hide();
	void curve_show(int l, int p);

	void marker_remove(int l, int p);

	QVector<QString> get_frontends();

	dvb_class *dvb;

	QwtPlotPicker *qwt_picker;

	spectrumscan_thread *myspectrumscan_thread;

private:
	QwtLegend *qwt_legend;
	QwtPlotGrid *qwt_grid;

signals:

public slots:
};

#endif // SPECTRUMSCAN_TAB_WORKER_H
