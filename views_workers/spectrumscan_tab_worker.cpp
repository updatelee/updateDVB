/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectrumscan_tab_worker.h"

spectrumscan_tab_worker::spectrumscan_tab_worker(dvb_class *d)
{
	dvb = d;

	myspectrumscan_thread = new spectrumscan_thread(dvb);

	qwt_legend = new QwtLegend;
	dvb->qwt_plot->insertLegend(qwt_legend, QwtPlot::RightLegend);
	dvb->qwt_plot->setAxisTitle(QwtPlot::xBottom, "Frequency");
	dvb->qwt_plot->setAxisTitle(QwtPlot::yLeft, "Amplitude");
	dvb->qwt_plot->setCanvasBackground(Qt::black);

	qwt_grid = new QwtPlotGrid();
	qwt_grid->enableX(true);
	qwt_grid->enableY(true);
	qwt_grid->enableXMin(true);
	qwt_grid->enableYMin(true);
	qwt_grid->setMajorPen(QPen(GRAY, 0, Qt::DotLine));
	qwt_grid->setMinorPen(QPen(DGRAY, 0, Qt::DotLine));
	qwt_grid->attach(dvb->qwt_plot);

	qwt_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn, qobject_cast<QwtPlotCanvas*>(dvb->qwt_plot->canvas()));
	qwt_picker->setStateMachine(new QwtPickerDragPointMachine());
	qwt_picker->setRubberBandPen(QColor(Qt::darkMagenta));
	qwt_picker->setRubberBand(QwtPicker::CrossRubberBand);
	qwt_picker->setTrackerPen(GREEN);
}

spectrumscan_tab_worker::~spectrumscan_tab_worker()
{
	while (dvb->dvb_thread->isRunning()) {
		dvb->dvb_thread->loop = false;
		dvb->dvb_thread->quit();
		dvb->dvb_thread->wait(100);
	}
	dvb->dvb_thread->deleteLater();

	while (myspectrumscan_thread->isRunning()) {
		myspectrumscan_thread->loop = false;
		myspectrumscan_thread->quit();
		myspectrumscan_thread->wait(100);
	}
	myspectrumscan_thread->deleteLater();
}

void spectrumscan_tab_worker::curve_hide()
{
	for (qwt_data *t : dvb->mc->qwt_lnb) {
		t->hide();
	}
}

void spectrumscan_tab_worker::curve_show(int l, int p)
{
	dvb->mc->qwt_lnb.at(l)->show(p);
}

void spectrumscan_tab_worker::marker_remove(int l, int p)
{
	for (QwtPlotMarker *t : dvb->mc->qwt_lnb.at(l)->qwt_pol.at(p)->qwt_marker) {
		t->detach();
		delete t;
	}
	dvb->mc->qwt_lnb.at(l)->qwt_pol.at(p)->qwt_marker.clear();
	dvb->qwt_plot->replot();
}

QVector<QString> spectrumscan_tab_worker::get_frontends()
{
	QVector<QString> f_num;
	QDir adapter_dir(QString("/dev/dvb/adapter%1").arg(dvb->dvb_thread->adapter));
	adapter_dir.setFilter(QDir::System|QDir::NoDotAndDotDot);
	QStringList frontend_entries = adapter_dir.entryList();
	frontend_entries = frontend_entries.filter("frontend");
	for (QString f : frontend_entries) {
		f.replace("frontend", "");
		f_num.append(f);
	}
	return f_num;
}

